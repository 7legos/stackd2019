﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReviver : FieldObject
{
    [SerializeField]
    private SpriteRenderer mySpriteRenderer;
    [SerializeField]
    private GameObject circle, borderCircle;

    private int playerID;
    private float timer = 999f;
    private float reviveImmuneTimer = 999f;
    private float maxTimer;

    public void Initialize(int playerID, float countdownTimer, Color color)
    {
        this.playerID = playerID;

        color.a = mySpriteRenderer.color.a;
        mySpriteRenderer.color = color;
        mySpriteRenderer.sortingOrder += playerID;
        color.a = circle.GetComponent<SpriteRenderer>().color.a;
        circle.GetComponent<SpriteRenderer>().color = color;
        circle.GetComponent<SpriteRenderer>().sortingOrder += playerID;
        color.a = borderCircle.GetComponent<SpriteRenderer>().color.a;
        borderCircle.GetComponent<SpriteRenderer>().color = color;
        borderCircle.GetComponent<SpriteRenderer>().sortingOrder += playerID;

        reviveImmuneTimer = 1.2f;
        timer = countdownTimer;
        maxTimer = countdownTimer;
    }

    void Update()
    {
        base.Update();
        if (GameplayManager.Instance.GetGameState() == GameState.GameOver)
            Destroy(this.gameObject);

        reviveImmuneTimer -= Time.deltaTime;

        if (reviveImmuneTimer < 0f && GameplayManager.Instance.GetClosestPlayerDistance(transform.position) < 4.35f)
        {
            timer += Time.deltaTime * 1.55f; //we are being revived
        }
        else
        {
            timer -= Time.deltaTime;
        }

        circle.transform.localScale = Vector3.one * Mathf.Lerp(0f, 1f, timer / maxTimer);
        if (timer < 0f)
        {
            Destroy(this.gameObject);
        }
        else if (timer > maxTimer + 0.05f)
        {
            GameplayManager.Instance.RespawnMultiPlayer(playerID);

            Destroy(this.gameObject);
        }
    }
}
