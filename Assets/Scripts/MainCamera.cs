﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour
{
    private Camera myCamera;
    private float aspectRatio;

    // Use this for initialization
    private void Awake()
    {
        myCamera = GetComponent<Camera>();

        aspectRatio = Screen.width / (float)Screen.height;
        myCamera.orthographicSize = -6.525f * aspectRatio + 21.4f;
        transform.position = new Vector3(17f, -6.328125f * aspectRatio + 20.56f, transform.position.z);
    }
}