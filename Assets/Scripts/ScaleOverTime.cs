﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleOverTime : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve scaleIn;
    [SerializeField]
    private float maxScale = 1f;
    [SerializeField]
    private float speed;

    private float lerper = 0f;
    
    void Update()
    {
        lerper += Time.deltaTime*speed;

        transform.localScale = Vector3.one * Mathf.Lerp(0f, maxScale, scaleIn.Evaluate(lerper));
    }
}
