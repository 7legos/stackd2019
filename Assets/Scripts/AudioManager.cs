﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private bool playBackgroundMusic;

    [SerializeField]
    private SoundEffectClip clickSound, redBoxExplosionSound, redBoxDisableSound, boxHitFloorSound, pinkBoxSpawnSound, orangeBoxSpawnSound, greenBoxSpawnSound,
        playerDeathSound, playerHitFloorSound, endBoxExplosionSound;

    [SerializeField]
    private Sprite[] soundToggle;

    [SerializeField]
    private Sprite[] soundTogglePlay;

    [SerializeField]
    private AudioSource[] myAudioSources;

    public static AudioManager Instance;

    private bool musicPaused = false;
    private Dictionary<string, AudioClip> musicNotes;
    private int audioSourcePlayIndex = 0; //which audiosource should we use (prevent sound overlaps)

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (Instance == null)
            Instance = this;
        else
            Debug.LogError("More than 1 AudioManager detected");
        // myAudioSource = GetComponent<AudioSource>();

        musicPaused = PlayerPrefs.GetInt("musicPaused", 0) == 1 ? true : false;
        //if (playBackgroundMusic)
        //    myAudioSource.Play();
        //myAudioSource.enabled = !musicPaused;
        //  myAudioSource.enabled = true;

        LoadMusicNotes();
    }

    private void LoadMusicNotes()
    {
        musicNotes = new Dictionary<string, AudioClip>();
        string[] noteNames = new string[]
        {
            "A#1",
            "A2",
            "A3",
            "C1",
            "D1",
            "D2",
            "D3",
            "D4",
            "D5",
            "E1",
            "F1",
            "G1",
        };

        for (int i = 0; i < noteNames.Length; i++)
        {
            AudioClip noteClip = Resources.Load<AudioClip>("MusicNotes/" + noteNames[i]);
            musicNotes.Add(noteNames[i], noteClip);
        }
    }

    public void ToggleMusic(Image soundUI, bool playScene)
    {
        //if (musicPaused)
        //{
        //    if (soundUI != null)
        //    {
        //        soundUI.sprite = soundToggle[0];
        //        if (playScene)
        //            soundUI.sprite = soundTogglePlay[0];
        //    }
        //    myAudioSource.enabled = true;
        //}
        //else
        //{
        //    if (soundUI != null)
        //    {
        //        soundUI.sprite = soundToggle[1];
        //        if (playScene)
        //            soundUI.sprite = soundTogglePlay[1];
        //    }
        //    myAudioSource.enabled = false;
        //}

        //musicPaused = !musicPaused;

        //PlayerPrefs.SetInt("musicPaused", musicPaused == true ? 1 : 0);
    }

    public void PlayClick()
    {
        PlaySoundEffect(SoundEffect.Click);
    }

    public void PlaySoundEffect(SoundEffect soundEffect)
    {
        switch (soundEffect)
        {
            case SoundEffect.Click:
                // PlaySoundEffectClip(clickSound);
                break;

            case SoundEffect.RedBoxExplosion:
                // PlaySoundEffectClip(redBoxExplosionSound);
                break;

            case SoundEffect.RedBoxDisable:
                // PlaySoundEffectClip(redBoxDisableSound);
                break;

            case SoundEffect.BoxHitFloor:
                // PlaySoundEffectClip(boxHitFloorSound);
                break;

            case SoundEffect.PinkBoxSpawn:
                // PlaySoundEffectClip(pinkBoxSpawnSound);
                break;

            case SoundEffect.OrangeBoxSpawn:
                // PlaySoundEffectClip(orangeBoxSpawnSound);
                break;

            case SoundEffect.GreenBoxSpawn:
                // PlaySoundEffectClip(greenBoxSpawnSound);
                break;

            case SoundEffect.PlayerDeath:
                // PlaySoundEffectClip(playerDeathSound);
                break;

            case SoundEffect.PlayerHitFloor:
                // PlaySoundEffectClip(playerHitFloorSound);
                break;

            case SoundEffect.EndBoxExplosion:
                // PlaySoundEffectClip(endBoxExplosionSound);
                break;
        }
    }

    private void PlaySoundEffectClip(SoundEffectClip soundEffect)
    {
        //if (soundEffect.audioClip != null && myAudioSource.enabled == true)
        //    myAudioSource.PlayOneShot(soundEffect.audioClip, soundEffect.volumeScale);
        //else
        //{
        //    //print("Audio clip is null: " + soundEffect);
        //}
    }

    public void SetUIButton(Image soundUI, bool playScene)
    {
        if (musicPaused)
        {
            if (soundUI != null)
            {
                soundUI.sprite = soundToggle[1];
                if (playScene)
                    soundUI.sprite = soundTogglePlay[1];
            }
        }
        else
        {
            if (soundUI != null)
            {
                soundUI.sprite = soundToggle[0];
                if (playScene)
                    soundUI.sprite = soundTogglePlay[0];
            }
        }
    }

    public void PlayMusicNote(MusicNoteData note)
    {
        string noteName = note.noteName.Substring(0, Mathf.Min(2, note.noteName.Length));

        if (musicNotes.ContainsKey(noteName))
        {
            myAudioSources[audioSourcePlayIndex].PlayOneShot(musicNotes[noteName], 0.3f);
            audioSourcePlayIndex = (audioSourcePlayIndex + 1) % 1;
        }
    }
}

[System.Serializable]
public class SoundEffectClip
{
    public AudioClip audioClip;

    [Range(0f, 15f)]
    public float volumeScale = 1f;
}

public enum SoundEffect
{
    Click,
    RedBoxExplosion,
    RedBoxDisable,
    BoxHitFloor,
    PinkBoxSpawn,
    OrangeBoxSpawn,
    GreenBoxSpawn,
    PlayerDeath,
    PlayerHitFloor,
    EndBoxExplosion
}