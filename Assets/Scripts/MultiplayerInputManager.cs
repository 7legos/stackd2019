﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerInputManager : MonoBehaviour
{
    public static MultiplayerInputManager Instance;
    [SerializeField]
    private KeyboardInputKeys[] defaultKeyboardInputs;

    private List<InputMethod> inputMethods;
    private Dictionary<int, int> playerInputs; //first int is playerIndex, second is index in inputMethods list

    private float inputMethodRefreshRate = 2f;
    private float refreshTimer = 0f;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (Instance == null)
            Instance = this;
        inputMethods = new List<InputMethod>();
        playerInputs = new Dictionary<int, int>();
    }

    void Start()
    {
    }

    void Update()
    {
        refreshTimer -= Time.deltaTime;
        if (refreshTimer < 0f)
        {
            refreshTimer = inputMethodRefreshRate;
            RefreshInputMethods();
        }
    }

    public void EnterLobby()
    {
        if (playerInputs.Count > 0)
        {
            for (int i = 0; i < playerInputs.Count; i++)
            {
                MultiplayerLobbyManager.Instance.AddPlayer(playerInputs[i]);
            }
        }
    }

    public void ClearAllPlayers()
    {
        playerInputs.Clear();
        RefreshInputMethods();
    }

    public bool CheckInput(int playerIndex, InputAction action)
    {
        if (playerInputs.ContainsKey(playerIndex))
        {
            switch (action)
            {
                case InputAction.Left:
                    return inputMethods[playerInputs[playerIndex]].CheckLeftInput();
                    break;
                case InputAction.Right:
                    return inputMethods[playerInputs[playerIndex]].CheckRightInput();
                    break;
                case InputAction.Jump:
                case InputAction.JumpDownPressOnly:
                    return inputMethods[playerInputs[playerIndex]].CheckJumpInput(action == InputAction.JumpDownPressOnly);
                    break;
                case InputAction.ToggleJumpKey:
                    return inputMethods[playerInputs[playerIndex]].CheckToggleJumpInput();
                    break;
            }
        }
        else
        {
            Debug.LogError("WARNING: Trying to check input of playerIndex: " + playerIndex + ", no input method found");
        }
        return false;
    }

    private void RefreshInputMethods()
    {
        inputMethods.Clear();
        for (int i = 0; i < defaultKeyboardInputs.Length; i++)
        {
            InputMethod defaultKeyboardInput = new InputMethod();
            defaultKeyboardInput.inputType = InputType.Keyboard;
            defaultKeyboardInput.AssignKeyboardKeys(defaultKeyboardInputs[i].leftKey, defaultKeyboardInputs[i].rightKey, defaultKeyboardInputs[i].jumpKey, defaultKeyboardInputs[i].toggleJumpKey);
            inputMethods.Add(defaultKeyboardInput);
        }

        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            InputMethod controllerInput = new InputMethod();
            controllerInput.inputType = InputType.USBController;
            controllerInput.SetControllerIndex(i);
            inputMethods.Add(controllerInput);
        }
    }

    public Color GetPlayerColor(int playerID)
    {
        switch (playerID)
        {
            case 0:
                return Color.green;
                break;
            case 1:
                return Color.cyan;
                break;
            case 2:
                return Color.magenta;
                break;
            case 3:
                return Color.yellow;
                break;

        }

        return Color.white;
    }

    public void CheckForJoiningPlayers()
    {
        for (int i = 0; i < inputMethods.Count; i++)
        {
            if (inputMethods[i].CheckJumpInput(true)) //check all available inputs for the join button
            {
                if (!playerInputs.ContainsValue(i)) //create a new player with these controls if they are not already taken by another player
                {
                    int newPlayerID = playerInputs.Count;
                    playerInputs.Add(newPlayerID, i);
                    MultiplayerLobbyManager.Instance.AddPlayer(newPlayerID);
                }
            }
        }
    }

    public string GetNextAvailableJoinButton()
    {
        for (int i = 0; i < inputMethods.Count; i++)
        {
            if (!playerInputs.ContainsValue(i))
            {
                return inputMethods[i].GetJumpKey().ToString();
            }
        }

        return "null";
    }

    public int CurrentNumberOfPlayers()
    {
        return playerInputs.Count;
    }
}



public struct InputMethod
{
    public InputType inputType;
    private int controllerIndex;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode jumpKey;
    private KeyCode toggleJumpKey;

    public bool CheckLeftInput()
    {
        switch (inputType)
        {
            case InputType.Keyboard:
                return Input.GetKey(leftKey);
                break;
            case InputType.USBController:
                return Input.GetAxis("Controller" + (controllerIndex + 1)) < -0.3f;
                break;
        }

        return false;
    }

    public bool CheckRightInput()
    {
        switch (inputType)
        {
            case InputType.Keyboard:
                return Input.GetKey(rightKey);
                break;
            case InputType.USBController:
                return Input.GetAxis("Controller" + (controllerIndex + 1)) > 0.3f;
                break;
        }

        return false;
    }

    public bool CheckJumpInput(bool downPressOnly)
    {
        switch (inputType)
        {
            case InputType.Keyboard:
                if (downPressOnly)
                    return Input.GetKeyDown(jumpKey);
                else
                    return Input.GetKey(jumpKey);
                break;
            case InputType.USBController:
                if (downPressOnly)
                {
                    if (Input.GetKeyDown(KeyCode.Joystick1Button0 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKeyDown(KeyCode.Joystick1Button1 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKeyDown(KeyCode.Joystick1Button2 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKeyDown(KeyCode.Joystick1Button3 + (controllerIndex * 20)))
                        return true;
                }
                else
                {
                    if (Input.GetKey(KeyCode.Joystick1Button0 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKey(KeyCode.Joystick1Button1 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKey(KeyCode.Joystick1Button2 + (controllerIndex * 20)))
                        return true;
                    if (Input.GetKey(KeyCode.Joystick1Button3 + (controllerIndex * 20)))
                        return true;
                }
                break;
        }

        return false;
    }

    public bool CheckToggleJumpInput()
    {
        switch (inputType)
        {
            case InputType.Keyboard:
                return Input.GetKeyDown(toggleJumpKey);
                break;
            case InputType.USBController:
                break;
        }

        return false;
    }

    public void SetControllerIndex(int index)
    {
        this.controllerIndex = index;
    }

    public KeyCode GetJumpKey()
    {
        return jumpKey;
    }

    public void AssignKeyboardKeys(KeyCode leftKey, KeyCode rightKey, KeyCode jumpKey, KeyCode toggleJumpKey)
    {
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.jumpKey = jumpKey;
        this.toggleJumpKey = toggleJumpKey;
    }
}

public enum InputType
{
    Keyboard,
    USBController
}

[System.Serializable]
public class KeyboardInputKeys
{
    public KeyCode leftKey;
    public KeyCode rightKey;
    public KeyCode jumpKey;
    public KeyCode toggleJumpKey;
}

public enum InputAction
{
    Left,
    Right,
    Jump,
    JumpDownPressOnly,
    ToggleJumpKey
}
