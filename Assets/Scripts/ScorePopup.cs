﻿using UnityEngine;
using System.Collections;

public class ScorePopup : FieldObject {
    [SerializeField]
    private float lifetime;

    private Box spawningBox;

    public void Init(Box spawningBox, string text, Color color)
    {
        TextMesh myText = GetComponent<TextMesh>();
        myText.text = text;
        myText.color = color;
        this.spawningBox = spawningBox;
    }

    protected override void Update()
    {
        base.Update();
        lifetime -= Time.deltaTime;
        if (lifetime <= 0f)
            Destroy(this.gameObject);

        if (spawningBox != null)
            transform.position = spawningBox.transform.position;

        transform.position = new Vector3(transform.position.x, transform.position.y, -5f);
    }
}
