﻿using UnityEngine;
using System.Collections;

public class Particle : FieldObject
{
    private ParticlePoolManager poolManager;
    private SpriteRenderer mySpriteRenderer;

    [SerializeField]
    private Vector3 startPosition;

    private float startAngle;
    private float startScale;

    [SerializeField]
    private Vector3 destinationPosition;

    private float destinationAngle;
    private float destinationScale;
    private float lerper = 0f;
    private float positionLerper = 0f;
    private Color color;
    private float dragDelta;
    private float drag;
    private float speed;

    public void Init(ParticlePoolManager manager)
    {
        DontDestroyOnLoad(this);
        poolManager = manager;
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        color = new Color(mySpriteRenderer.color.r, mySpriteRenderer.color.g, mySpriteRenderer.color.b, mySpriteRenderer.color.a);
    }

    protected override void Update()
    {
        base.Update();
        lerper += Time.deltaTime * speed;
        positionLerper += Time.deltaTime * Mathf.Clamp(speed - drag, 0f, float.MaxValue);
        drag += dragDelta * Time.deltaTime;

        color.a = 1f - positionLerper;
        mySpriteRenderer.color = color;
        fieldPosition = Vector3.Lerp(startPosition, destinationPosition, positionLerper);
        transform.rotation = Quaternion.Lerp(Quaternion.AngleAxis(destinationAngle, Vector3.forward), Quaternion.AngleAxis(startAngle, Vector3.forward), lerper);
        transform.localScale = Vector3.Lerp(Vector3.one * startScale, Vector3.one * destinationScale, positionLerper);

        if (positionLerper >= 1f || lerper > 5f)
        {
            Deactivate();
        }
    }

    protected override void OnOffset(float lastOffset)
    {
        destinationPosition -= Vector3.up * lastOffset;
        startPosition -= Vector3.up * lastOffset;
    }

    public void Activate(Vector3 startPosition, Vector3 destinationPosition, float scale, float rotationAngle, Color color, float speed, float dragDelta, float initDrag)
    {
        mySpriteRenderer.color = color;
        float randomRotation = 180f - Random.Range(-90f, 90f);
        transform.rotation = Quaternion.AngleAxis(randomRotation, Vector3.forward);
        transform.localScale = new Vector3(scale, scale, scale);
        fieldPosition = startPosition;
        startScale = scale;
        this.startPosition = startPosition;
        this.destinationPosition = destinationPosition;
        this.color = color;
        this.dragDelta = dragDelta;
        destinationScale = scale / 6f;
        destinationAngle = randomRotation + rotationAngle;
        lerper = 0f;
        drag = initDrag;
        positionLerper = 0;
        this.speed = speed;
    }

    public void Deactivate()
    {
        poolManager.Push(this);
    }
}