﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistantSettings : MonoBehaviour
{
    public static PersistantSettings Instance;

    private bool multiplayerMode = false;
    private GameMode gameMode;
    private GameMode lastPlayerChosenGameMode;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        DontDestroyOnLoad(this.gameObject);
        if (Instance == null)
            Instance = this;
    }

    public void SetMultiplayerMode(bool multiplayer)
    {
        multiplayerMode = multiplayer;
    }

    public bool IsMultiplayerMode()
    {
        return multiplayerMode;
    }

    public bool IsGameMode(GameMode gameMode)
    {
        return this.gameMode == gameMode;
    }

    public GameMode GetGameMode()
    {
        return gameMode;
    }

    public void SetGameMode(GameMode gameMode)
    {
        this.gameMode = gameMode;
    }

    public void SetLastPlayerChosenGameMode(GameMode gameMode)
    {
        lastPlayerChosenGameMode = gameMode;
    }

    public GameMode GetLastPlayerChosenGameMode()
    {
        return lastPlayerChosenGameMode;
    }
}