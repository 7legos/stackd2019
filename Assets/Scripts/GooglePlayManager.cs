﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using System;
using UnityEngine.SocialPlatforms;

public class GooglePlayManager : MonoBehaviour
{
    public static GooglePlayManager Instance;
    public static string LeaderboardID = "CgkIvrmyrvkNEAIQAA";//CgkIs8Sal48VEAIQAQ

    void Awake()
    {
        Instance = this;

        //      PlayGamesPlatform.Activate();
    }

    void Start()
    {

        if (PlayerPrefs.GetInt("GPauthenticated", 0) == 1)
            Social.localUser.Authenticate(OnAuthenticate);

        if (PlayerPrefs.GetInt("GPauthenticatedInit", 0) == 0)
        {
            PlayerPrefs.SetInt("GPauthenticatedInit", 1);
            Social.localUser.Authenticate(OnAuthenticate);
        }

    }

    private void OnAuthenticate(bool success)
    {
        ILeaderboard leaderboard = Social.CreateLeaderboard();
        leaderboard.id = LeaderboardID;

        if (success)
            PlayerPrefs.SetInt("GPauthenticated", 1);
        else
            PlayerPrefs.SetInt("GPauthenticated", 0);

        leaderboard.LoadScores(ok =>
        {
            if (ok)
            {
                if (leaderboard.localUserScore.value > PlayerPrefs.GetInt("highScore", 0))
                {
                    PlayerPrefs.SetInt("highScore", (int)leaderboard.localUserScore.value);
                }
            }
        });

    }

    public void ShowLeaderboard()
    {
        if (Social.localUser.authenticated)
            Social.Active.ShowLeaderboardUI();
        else
            Social.localUser.Authenticate(OnAuthenticate);
    }
}
