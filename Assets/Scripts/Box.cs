﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct IntVector2
{
    public int x;
    public int y;

    public IntVector2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

public class Box : FieldObject
{
    [SerializeField]
    protected BoxColor boxColor;

    [SerializeField]
    protected Beam beamPrefab;

    [SerializeField]
    protected Box spawnBoxPrefab;

    [SerializeField]
    protected GameObject shardPrefab;

    [SerializeField]
    protected int spawnPower;

    [SerializeField]
    public IntVector2[] spawnPositions;

    [SerializeField]
    protected Box[] boostSpawnBoxPrefab;

    [SerializeField]
    protected GameObject[] boostShards;

    public int ColumnIndex;
    public int DestinationRowIndex { set { if (value >= 0) destinationRowIndex = value; } get { return destinationRowIndex; } }
    public bool IsActivated = false;
    public bool CanGivePoint = false;
    public bool IsBoostBox = false; //Is a block for boost (for the colors)

    private bool isSpawnedBox = false;

    public bool InAir { get { return inAir; } }
    public float CurrentVelocity { get { return currentVelocity; } }

    [SerializeField]
    private int destinationRowIndex;

    protected SpriteRenderer mySpriteRenderer;
    protected Beam myBeam;
    private float activationTimer;
    protected GameObject[] spawnShards;
    protected Vector3[] spawnShardDestinationOffset;
    protected float spawnShardMaxLerp;
    protected Vector3 spawnShardInitialPosition;

    [SerializeField]
    protected int deactivationRowIndex = 0;

    [SerializeField]
    protected float currentVelocity;

    protected float currentVelocityDelta = 0f; //Additive Acceleration
    protected bool inAir = true;
    protected bool hasSpawnedBoxes = false; //Have we spawned our boxes?
    private bool canScore = true;

    protected override void Awake()
    {
        base.Awake();
        currentVelocity = GameplayManager.BOX_START_VELOCITY / 60f;
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    //protected override void Update()
    //{
    //    base.Update();
    //    if (!IsActivated)
    //    {
    //        activationTimer -= Time.deltaTime;

    //        MoveShards();
    //        if (activationTimer <= 0f)
    //        {
    //            ActivateBox();
    //            for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
    //            {
    //                Destroy(spawnShards[i].gameObject);
    //            }
    //        }
    //    }
    //}

    private void MoveShards()
    {
        for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
        {
            if (spawnShards[i] != null)
                spawnShards[i].transform.position = Vector3.Lerp(transform.position + spawnShardDestinationOffset[i], spawnShardInitialPosition, activationTimer / spawnShardMaxLerp);
        }
    }

    public void UpdateStep(float scale)
    {
        if (!IsActivated)
        {
            MoveShards();
            activationTimer -= 0.012f * scale;

            if (activationTimer <= 0f)
            {
                ActivateBox();
                for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
                {
                    Destroy(spawnShards[i].gameObject);
                }
            }
        }
    }

    public void MoveStep(float scale)
    {
        Box otherBox = null;
        if (!IsActivated)
            fieldPosition = new Vector2(ColumnIndex, deactivationRowIndex);

        //if (fieldPosition.y <= DestinationRowIndex && InAir)
        //    OnLanded();

        if (InAir && IsActivated) //If we are still falling (havent reached destination row yet)
        {
            currentVelocity += (GameplayManager.BOX_VELOCITY_DELTA / 1900f * scale) + currentVelocityDelta;
            currentVelocityDelta += GameplayManager.BOX_VELOCITY_DELTA_DELTA / 4300f * scale;

            fieldPosition = new Vector2(ColumnIndex, fieldPosition.y - (currentVelocity * scale));

            if (GameplayManager.Instance.CheckLandedOnBox(ColumnIndex, this, transform.position, out otherBox))
            {
                fieldPosition = otherBox.fieldPosition + (Vector2.up);
                currentVelocity = otherBox.currentVelocity;
                currentVelocityDelta = otherBox.currentVelocityDelta;

                if (otherBox.InAir == false)
                {
                    if (otherBox.DestinationRowIndex < GameplayManager.MAX_ROWS - 1)
                    {
                        DestinationRowIndex = otherBox.DestinationRowIndex + 1;
                        deactivationRowIndex = DestinationRowIndex;
                    }
                }
            }
        }

        if (fieldPosition.y <= Mathf.Min(DestinationRowIndex, deactivationRowIndex) + GameplayManager.EPSILON && InAir)
            OnLanded();

        if (!InAir)
        {
            currentVelocity = 0f;
            fieldPosition = new Vector2(ColumnIndex, Mathf.Min(DestinationRowIndex, deactivationRowIndex));
        }
    }

    private void OnLanded()
    {
        currentVelocity = 0f;
        inAir = false;
        if (!isSpawnedBox && GameplayManager.Instance.GetGameState() != GameState.MenuWithFallingBlocks)
            AudioManager.Instance.PlaySoundEffect(SoundEffect.BoxHitFloor);

        GameplayManager.Instance.ColumnUpOffset(ColumnIndex, this); //moved from OnLanded
        fieldPosition = new Vector3(ColumnIndex, DestinationRowIndex);
        UpdateBeams();

        if (CanGivePoint && IsActivated)
        {
            CanGivePoint = false;
            GameplayManager.Instance.BoxLanded(this, canScore);
        }

        if (spawnPositions.Length > 0 && !hasSpawnedBoxes && IsActivated)
            SpawnBoxes();
    }

    public void Fall()
    {
        if (!InAir)
        {
            inAir = true;
            currentVelocity = 0f;
            currentVelocityDelta = 0.0f;
        }
    }

    public void SpawnBoostBoxes(int count)
    {
        IsBoostBox = true;
        hasSpawnedBoxes = false;
        spawnPower = count;
        SpawnBoxes();
    }

    //Should only called when we landed, not in the air
    private void SpawnBoxes()
    {
        if (spawnPower <= 0)
            return;

        hasSpawnedBoxes = true;
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            int spawnIndexX = ColumnIndex + spawnPositions[i].x;
            int spawnIndexY = Mathf.RoundToInt(fieldPosition.y) + spawnPositions[i].y;

            if (GameplayManager.Instance.IsSpawnSpaceFree(spawnIndexX, spawnIndexY))
            {
                Box spawnedBox = null;
                //if (IsBoostBox)
                //    spawnedBox = Instantiate(boostSpawnBoxPrefab[spawnPower % 5]) as Box;
                //else
                spawnedBox = Instantiate(spawnBoxPrefab) as Box;

                float spawnActivationTime = 0.1f; //how fast box animations go, will make orange faster
                if (boxColor == BoxColor.Orange)
                    spawnActivationTime = 0.065f;
                spawnedBox.fieldPosition = new Vector2(spawnIndexX, spawnIndexY);
                spawnedBox.InitSpawned(spawnIndexX, GameplayManager.Instance.GetLowestFreeRowIndex(spawnIndexX), transform.position, spawnActivationTime, spawnPower - 1);
                spawnedBox.SetCanScore(canScore);

                if (IsBoostBox)
                {
                    spawnedBox.IsBoostBox = true;
                }

                if (spawnIndexX < ColumnIndex) //If we spawn a block to the left, make sure it cant spawn to the right any more
                {
                    for (int j = 0; j < spawnedBox.spawnPositions.Length; j++)
                    {
                        if (spawnedBox.spawnPositions[j].x > 0)
                        {
                            spawnedBox.spawnPositions[j] = new IntVector2(int.MaxValue, int.MaxValue);
                        }
                    }
                }

                if (spawnIndexX > ColumnIndex) //If we spawn a block to the right, make sure it cant spawn to the left any more
                {
                    for (int j = 0; j < spawnedBox.spawnPositions.Length; j++)
                    {
                        if (spawnedBox.spawnPositions[j].x < 0)
                        {
                            spawnedBox.spawnPositions[j] = new IntVector2(int.MaxValue, int.MaxValue);
                        }
                    }
                }
            }
        }
    }

    public void Init(int newColumnIndex, int newDestinationRowIndex, bool spawnBeam)
    {
        ColumnIndex = newColumnIndex;
        DestinationRowIndex = newDestinationRowIndex;
        // gameObject.layer = 8 + ColumnIndex;
        GameplayManager.Instance.RegisterBox(this, newColumnIndex);
        CanGivePoint = true;
        IsActivated = true;

        if (spawnBeam)
        {
            myBeam = Instantiate(beamPrefab, new Vector3(ColumnIndex, GameplayManager.SPAWN_HEIGHT), Quaternion.identity) as Beam;
            myBeam.Init(ColumnIndex, DestinationRowIndex);
        }
    }

    //spawnTime is the time the 'shards' take to converge and form the block (and have the block show)
    //spawnPower is how many blocks left this block can spawn (ie. vertical box can spawn a few, orange is set to a high number for unlimited)
    public void InitSpawned(int newColumnIndex, int newDestinationRowIndex, Vector3 shardStartLocation, float activationTimer, int spawnPower)
    {
        if (transform.childCount > 0)
            Destroy(transform.GetChild(0).gameObject);

        isSpawnedBox = true;
        CanGivePoint = false;
        ColumnIndex = newColumnIndex;
        DestinationRowIndex = newDestinationRowIndex;
        // gameObject.layer = 8 + ColumnIndex;
        GameplayManager.Instance.RegisterBox(this, newColumnIndex);
        DeactivateBox();
        this.activationTimer = activationTimer;
        spawnShards = new GameObject[GameplayManager.SPAWN_SHARD_COUNT];
        spawnShardInitialPosition = shardStartLocation;
        spawnShardDestinationOffset = new Vector3[GameplayManager.SPAWN_SHARD_COUNT];
        spawnShardMaxLerp = activationTimer;
        if (IsBoostBox)
            deactivationRowIndex = newDestinationRowIndex;
        else
            deactivationRowIndex = Mathf.RoundToInt(fieldPosition.y);

        this.spawnPower = spawnPower;

        for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
        {
            //if (IsBoostBox)
            //    spawnShards[i] = Instantiate(boostShards[spawnPower % 5], shardStartLocation, Quaternion.identity) as GameObject;
            //else
            spawnShards[i] = Instantiate(shardPrefab, shardStartLocation, Quaternion.identity) as GameObject;

            spawnShardDestinationOffset[i] = new Vector3(Random.Range(-0.4f, 0.4f), Random.Range(-0.4f, 0.4f));
        }
    }

    private void DeactivateBox()
    {
        IsActivated = false;
        mySpriteRenderer.enabled = false;
    }

    private void ActivateBox()
    {
        IsActivated = true;
        mySpriteRenderer.enabled = true;
        CanGivePoint = true;
        GameplayManager.Instance.UpdateBeams(ColumnIndex);
        if (!hasSpawnedBoxes)
            SpawnBoxes();

        if (GameplayManager.Instance.GetGameState() != GameState.MenuWithFallingBlocks)
        {
            switch (boxColor)
            {
                case BoxColor.Orange:
                    AudioManager.Instance.PlaySoundEffect(SoundEffect.OrangeBoxSpawn);
                    break;

                case BoxColor.Pink:
                    AudioManager.Instance.PlaySoundEffect(SoundEffect.PinkBoxSpawn);
                    break;

                case BoxColor.Green:
                    AudioManager.Instance.PlaySoundEffect(SoundEffect.GreenBoxSpawn);
                    break;
            }
        }

        if (CanGivePoint && IsActivated)
        {
            CanGivePoint = false;
            GameplayManager.Instance.BoxLanded(this, canScore);
        }
    }

    public void UpdateBeams()
    {
        if (myBeam != null)
            myBeam.DestinationRowIndex = GameplayManager.Instance.GetHighestLandedColumnBoxY(ColumnIndex);
    }

    public void UpOffset()
    {
        // DestinationRowIndex++;

        if (myBeam != null)
            myBeam.DestinationRowIndex = GameplayManager.Instance.GetHighestLandedColumnBoxY(ColumnIndex);
    }

    public void DownOffset()
    {
        DestinationRowIndex--;
        deactivationRowIndex--;
        if (myBeam != null)
            myBeam.DestinationRowIndex = GameplayManager.Instance.GetHighestLandedColumnBoxY(ColumnIndex);
    }

    //Index is from the GameplayManager inspector
    public void ChangeBoxType(int boxIndex)
    {
        GameplayManager.Instance.UnregisterBox(this, ColumnIndex);
        Box spawnedBox = Instantiate(GameplayManager.Instance.BoxSpawns[boxIndex].box, transform.position, Quaternion.identity) as Box;
        spawnedBox.fieldPosition = fieldPosition;
        spawnedBox.Init(ColumnIndex, GameplayManager.Instance.GetClosestBoxRow(fieldPosition), false);
        spawnedBox.CanGivePoint = CanGivePoint;

        GameplayManager.Instance.UpdateColumn(ColumnIndex, DestinationRowIndex);
        Destroy(this.gameObject);
    }

    public void StopSpawns()
    {
        if (!IsActivated)
            Destroy(this.gameObject);
        for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
        {
            if (spawnShards != null && spawnShards[i] != null)
                Destroy(spawnShards[i].gameObject);
        }
    }

    public virtual void DestroyBox(bool chainExplosion, bool playParticles, bool endingTransition, bool unregister = true, float delay = 0f)
    {
        if (myBeam != null)
            myBeam.QuickFadeOut();

        if (unregister)
            GameplayManager.Instance.UnregisterBox(this, ColumnIndex);
        else
        {
            for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
            {
                if (spawnShards != null && spawnShards[i] != null)
                    Destroy(spawnShards[i].gameObject);
            }
            if (IsActivated)
                StartCoroutine(DestroyMenuBoxWithDelay(delay));
            else
            {
                Destroy(this.gameObject);
            }
            return;
        }

        if (chainExplosion)
        {
            GameplayManager.Instance.BoxExploded(this, canScore);
        }

        if (playParticles)
            GameplayManager.Instance.CreateBoxParticles(fieldPosition, mySpriteRenderer.color);
        else
        if (endingTransition)
            GameplayManager.Instance.CreateEndingBoxParticles(fieldPosition, mySpriteRenderer.color);

        for (int i = 0; i < GameplayManager.SPAWN_SHARD_COUNT; i++)
        {
            if (spawnShards != null && spawnShards[i] != null)
                Destroy(spawnShards[i].gameObject);
        }

        Destroy(this.gameObject);
    }

    private IEnumerator DestroyMenuBoxWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameplayManager.Instance.CreateEndingBoxParticles(fieldPosition, mySpriteRenderer.color);

        Destroy(this.gameObject);
    }

    public void SetCanScore(bool canScore)
    {
        this.canScore = canScore;
        CanGivePoint = false;
    }
}

public enum BoxColor
{
    None,
    Blue,
    Orange,
    Pink,
    Green
}