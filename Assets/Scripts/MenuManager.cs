﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum MenuState
{
    Intro,
    Waiting,
    Transitioning
}

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private float logoWaitTime;

    [SerializeField]
    private Image logo;

    [SerializeField]
    private Image soundUI;

    [SerializeField]
    private Text currentScore;

    [SerializeField]
    private Text highScore;

    [SerializeField]
    private Image initOverlay;

    [SerializeField]
    private Image overlay;

    [SerializeField]
    private GameObject settingsMenu;

    [SerializeField]
    private GameObject creditsMenu;

    private MenuState menuState;

    private float alpha = 1f;
    private bool isFading = false;
    private bool isFadingIn = false;
    private bool isInit;

    private void Awake()
    {
        Time.timeScale = 1f;
        isInit = PlayerPrefs.GetInt("init") == 1;
        if (!isInit)
            Destroy(logo.gameObject);
    }

    private void Start()
    {
        currentScore.text = "" + PlayerPrefs.GetInt("currentScore", 0);
        highScore.text = "" + PlayerPrefs.GetInt("highScore", 0);
        // ToggleMusic(); ToggleMusic();
        SetSoundUIButton();
        menuState = MenuState.Waiting;
        FadeIn();
    }

    private IEnumerator Intro()
    {
        yield return new WaitForSeconds(logoWaitTime);
    }

    private void Update()
    {
        if (isFading)
        {
            if (isFadingIn) //fading in
            {
                if (isInit && logoWaitTime > 0f)
                {
                    initOverlay.color = new Color(initOverlay.color.r, initOverlay.color.g, initOverlay.color.b, alpha);
                    logoWaitTime -= Time.deltaTime;
                    if (logoWaitTime <= 0f)
                        Destroy(logo.gameObject);
                }
                else //after logo
                {
                    if (alpha > 0f)
                    {
                        alpha -= Time.deltaTime;

                        overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, alpha);

                        if (isInit)
                        {
                            initOverlay.color = new Color(initOverlay.color.r, initOverlay.color.g, initOverlay.color.b, alpha);
                        }

                        if (alpha < 0.7f)
                        {
                            overlay.raycastTarget = false;
                            initOverlay.raycastTarget = false;
                        }
                    }
                    else
                    {
                        isFading = false;
                        menuState = MenuState.Waiting;
                    }
                }
            }
            else //fading out
            {
                if (alpha < 1f)
                {
                    alpha += Time.deltaTime;

                    overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, alpha);
                }
                else
                {
                    isFading = false;
                }
            }
        }
    }

    private void FadeIn()
    {
        // StopCoroutine(FadeOut());
        isFading = true;
        isFadingIn = true;
        alpha = 1f;

        // Destroy(logo.gameObject);

        // CanvasGroup initCanvasGroup = initOverlay.GetComponent<CanvasGroup>();
    }

    private void FadeOut()
    {
        // StopCoroutine(FadeIn());
        isFading = true;
        isFadingIn = false;
        PlayerPrefs.SetInt("init", 0);
        menuState = MenuState.Transitioning;
        // float alpha = 0f;
    }

    public void EnableCredits()
    {
        creditsMenu.SetActive(true);
    }

    public void DisableCredits()
    {
        creditsMenu.SetActive(false);
    }

    public void OpenSettings()
    {
        settingsMenu.SetActive(true);
    }

    public void CloseSettings()
    {
        settingsMenu.SetActive(false);
    }

    public void ToggleMusic()
    {
        AudioManager.Instance.ToggleMusic(soundUI, false);
    }

    private void SetSoundUIButton()
    {
        AudioManager.Instance.SetUIButton(soundUI, false);
    }

    public void ButtonClick()
    {
        AudioManager.Instance.PlayClick();
    }

    private IEnumerator FadeToScene(string sceneName)
    {
        FadeOut();
        while (isFading)
            yield return null;
        SceneManager.LoadScene(sceneName);
    }

    public void LoadPlayScene()
    {
        if (menuState == MenuState.Waiting)
        {
            PersistantSettings.Instance.SetMultiplayerMode(false);
            PersistantSettings.Instance.SetGameMode(GameMode.Endless);
            StartCoroutine(FadeToScene("Play"));
        }
    }

    public void LoadPlaySceneNonEndlessMode()
    {
        if (menuState == MenuState.Waiting)
        {
            PersistantSettings.Instance.SetMultiplayerMode(false);
            PersistantSettings.Instance.SetGameMode(GameMode.LevelFromFile);
            StartCoroutine(FadeToScene("Play"));
        }
    }

    public void LoadMultiplayerLobby()
    {
        if (menuState == MenuState.Waiting)
        {
            PersistantSettings.Instance.SetGameMode(GameMode.Endless);
            StartCoroutine(FadeToScene("MultiplayerLobby"));
        }
    }
}