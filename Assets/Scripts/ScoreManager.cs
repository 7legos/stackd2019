﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private ScorePopup scorePopup;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text highScoreText;

    private int score;
    private int highScore;

    public int Score { get { return score; } set { score = value; scoreText.text = "" + score; if (score > highScore) highScoreText.text = "" + score; } }

    void Awake()
    {
        highScore = PlayerPrefs.GetInt("highScore", 0);
        highScoreText.text = "" + highScore;
        score = 0;
        scoreText.text = "0";
    }

    public void CreateScorePopop(Box box, string text, Color color)
    {
        Vector3 popupPosition = new Vector3(box.transform.position.x, box.transform.position.y, scorePopup.transform.position.z);
        ScorePopup popup = Instantiate(scorePopup, popupPosition, Quaternion.identity) as ScorePopup;
        popup.fieldPosition = box.fieldPosition;
        popup.Init(box, text, color);
    }
}
