﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LogoScene : MonoBehaviour {

    [SerializeField]
    private float waitTime;

    private float timer;
    private AsyncOperation loading;

    void Start()
    {
        timer = waitTime;
       loading = SceneManager.LoadSceneAsync("Transition");
        loading.allowSceneActivation = false;
    }

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0f)
            loading.allowSceneActivation = true;
    }
}
