﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Instance;

    private static List<string>[] levelRows;
    private static List<string>[] levelMusicRows;

    private static string levelsPath;
    private static string musicTestLevel = "MUSIC_LEVEL_AXEL_1";
    private static int currentLevel;

    private void Awake()
    {
        Instance = this;

        levelsPath = Application.dataPath + "/Resources/LevelsData";
    }

    public static void LoadLevel(int levelNumber)
    {
        currentLevel = levelNumber;
        if (DoesFileExactStringExist(levelsPath + "/" + levelNumber + " - Sheet1.csv"))
            levelRows = CSVReader.GetMultipleRows(levelsPath + "/" + levelNumber + " - Sheet1.csv", 1, 9999);
        else
            levelRows = CSVReader.GetMultipleRows(levelsPath + "/" + levelNumber + "_-_Sheet1.csv", 1, 9999);
    }

    public static void LoadMusicLevel()
    {
        if (DoesFileExactStringExist(levelsPath + "/" + musicTestLevel + " - Sheet1.csv"))
            levelMusicRows = CSVReader.GetMultipleRows(levelsPath + "/" + musicTestLevel + " - Sheet1.csv", 2, 9999);
        else
            levelMusicRows = CSVReader.GetMultipleRows(levelsPath + "/" + musicTestLevel + "_-_Sheet1.csv", 2, 9999);
    }

    public static void ReloadLevel()
    {
        switch (PersistantSettings.Instance.GetGameMode())
        {
            case GameMode.LevelFromFile:
                LoadLevel(currentLevel);
                break;

            case GameMode.MusicLevelFromFile:
                LoadMusicLevel();
                break;
        }
    }

    public static List<string>[] GetLevelRows()
    {
        return levelRows;
    }

    public static List<string>[] GetLevelMusicRows()
    {
        return levelMusicRows;
    }

    public static bool DoesLevelFileExist(int levelNumber)
    {
        return DoesFileExactStringExist(levelsPath + "/" + levelNumber + " - Sheet1.csv") || DoesFileExactStringExist(levelsPath + "/" + levelNumber + "_-_Sheet1.csv");
    }

    private static bool DoesFileExactStringExist(string filePath)
    {
        return File.Exists(filePath);
    }
}