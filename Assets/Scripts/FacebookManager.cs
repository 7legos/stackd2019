﻿using UnityEngine;
using System.Collections;
//using Facebook.Unity;
using System;
using System.Collections.Generic;

public class ProfileResult
{
    public string name;
    public string id;
}

public class ProfileScore
{
    public long score;
    public string name;
    public string id;
    public int place;
    public Texture2D profilePic;
}

public class FacebookManager : MonoBehaviour
{
    // [SerializeField]
    // private GameObject connectButton;
    // [SerializeField]
    // private GameObject listContent;
    // [SerializeField]
    // private ProfileList profileListPrefab;
    // [SerializeField]
    // private GameObject lowerClamp, upperClamp;
    // [SerializeField]
    // private GameObject canvasRoot;

    // private static List<ProfileScore> scores;
    // private static string myID;

    // private List<ProfileList> scoreListObjects;

    // private ProfileList myProfileList;
    // private ProfileList myClampedProfileList;

    // // Use this for initialization
    // void Awake()
    // {
    //     scoreListObjects = new List<ProfileList>();

    //     if (!FB.IsInitialized)
    //     {
    //         FB.Init(OnInitComplete);
    //         scores = new List<ProfileScore>();
    //     }
    //     else if (FB.IsLoggedIn)
    //     {
    //         CreateListUI();
    //     }
    // }

    // void Update()
    // {
    //     ClampMyProfileList();
    // }

    // private void ClampMyProfileList()
    // {
    //     if (myClampedProfileList != null)
    //     {
    //         myClampedProfileList.transform.SetParent(canvasRoot.transform);
    //         myClampedProfileList.transform.localScale = Vector3.one;

    //         myClampedProfileList.transform.position = myProfileList.transform.position;

    //         if (myClampedProfileList.transform.position.y > upperClamp.transform.position.y)
    //             myClampedProfileList.transform.position = upperClamp.transform.position;

    //         if (myClampedProfileList.transform.position.y < lowerClamp.transform.position.y)
    //             myClampedProfileList.transform.position = lowerClamp.transform.position;
    //     }
    // }

    // private void OnInitComplete()
    // {
    //     if (FB.IsLoggedIn)
    //     {
    //         LoadInfo();
    //     }
    // }

    // public void Login()
    // {
    //     //    FB.LogInWithPublishPermissions(new List<string>() { "publish_actions", "user_friends" }, OnReceiveLogin);
    //     FB.LogInWithReadPermissions(new List<string>() { "user_friends" }, OnReceiveLogin);

    // }

    // public void OpenPage()
    // {
    //     Application.OpenURL("https://www.facebook.com/StackDGame/");
    // }

    // private void OnReceiveLogin(ILoginResult result)
    // {
    //     if (FB.IsLoggedIn)
    //     {
    //         LoadInfo();
    //     }
    // }

    // private void LoadInfo()
    // {
    //     FB.API("/me", HttpMethod.GET, GetMyProfile);
    //     FB.API("/" + FB.AppId + "/scores", HttpMethod.GET, GetScores);
    // }

    // private void GetMyProfile(IGraphResult result)
    // {
    //     ProfileResult pr = JsonUtility.FromJson<ProfileResult>(result.RawResult);
    //     myID = pr.id;
    //     CheckMyProfileList();
    // }

    // private void GetScores(IGraphResult result)
    // {
    //     if (result.ResultDictionary.ContainsKey("error"))
    //     {
    //         Debug.LogError(result.RawResult);
    //         return;
    //     }

    //     var dataList = result.ResultDictionary["data"] as List<object>;

    //     if (dataList.Count > 0)
    //     {
    //         for (int i = 0; i < dataList.Count; i++)
    //         {
    //             Dictionary<string, object> profileResult = dataList[i] as Dictionary<string, object>;

    //             ProfileScore profileScore = new ProfileScore();
    //             profileScore.score = (long)profileResult["score"];
    //             profileScore.name = ((profileResult["user"] as Dictionary<string, object>)["name"]).ToString();
    //             profileScore.id = ((profileResult["user"] as Dictionary<string, object>)["id"]).ToString();
    //             profileScore.place = i + 1;

    //             LoadProfilePic(profileScore.id);

    //             if (profileScore.id.Equals(myID))
    //                 CheckHighScore((int)profileScore.score);

    //             scores.Add(profileScore);
    //         }
    //         CreateListUI();
    //     }
    // }

    // private void CheckHighScore(int facebookScore)
    // {
    //     int currentHighscore = PlayerPrefs.GetInt("highScore", 0);
    //     int currentHighscoreFB = PlayerPrefs.GetInt("currentHighScoreFB", 0);

    //     if (facebookScore > currentHighscore)
    //         PlayerPrefs.SetInt("highScore", facebookScore);

    //     PlayerPrefs.SetInt("currentHighScoreFB", facebookScore);
    // }

    // private void CreateListUI()
    // {
    //     connectButton.SetActive(false);
    //     listContent.SetActive(true);

    //     if (scores.Count > 0)
    //     {
    //         for (int i = 0; i < scores.Count; i++)
    //         {
    //             ProfileList profileList = Instantiate(profileListPrefab) as ProfileList;
    //             ProfileScore profileScore = scores[i];

    //             profileList.transform.SetParent(listContent.transform);
    //             profileList.SetInfo(profileScore.place, profileScore.name, profileScore.score, profileScore.id);

    //             if(scores[i].profilePic!=null)
    //             {
    //                 profileList.SetPicture(scores[i].profilePic);
    //             }

    //             scoreListObjects.Add(profileList);
    //         }
    //         CheckMyProfileList();
    //     }
    // }

    // private void CheckMyProfileList()
    // {
    //     if (scoreListObjects.Count > 0)
    //     {
    //         for (int i = 0; i < scoreListObjects.Count; i++)
    //         {
    //             if (scoreListObjects[i].profileID.Equals(myID))
    //             {
    //                 scoreListObjects[i].SetMyProfile();
    //                 myProfileList = scoreListObjects[i];
    //                 myClampedProfileList = Instantiate(scoreListObjects[i]);
    //             }
    //         }
    //     }
    // }

    // private void LoadProfilePic(string profileID)
    // {
    //     StartCoroutine(DownloadProfilePic(profileID, "https://graph.facebook.com/" + profileID + "/picture?type=normal"));
    // }

    // private IEnumerator DownloadProfilePic(string id, string url)
    // {
    //     WWW www = new WWW(url);

    //     yield return www;

    //     Texture2D profilePic = www.texture;

    //     if (scoreListObjects.Count > 0)
    //     {
    //         for (int i = 0; i < scoreListObjects.Count; i++)
    //         {
    //             if(scoreListObjects[i].profileID.Equals(id))
    //             {
    //                 scoreListObjects[i].SetPicture(profilePic);
    //             }

    //             if(myClampedProfileList.profileID.Equals(id))
    //             {
    //                 myClampedProfileList.SetPicture(profilePic);
    //             }
    //         }
    //     }

    //     if (scores.Count > 0)
    //     {
    //         for (int i = 0; i < scores.Count; i++)
    //         {
    //             if (scores[i].id.Equals(id))
    //             {
    //                 scores[i].profilePic = profilePic;
    //             }
    //         }
    //     }
    // }
}
