﻿using UnityEngine;
using System.Collections;
using admob;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;
    private Admob ad;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);
        if (Instance == null)
            Instance = this;
        else
            Debug.LogError("More than 1 AdsManager detected");
        ad = Admob.Instance();
        #if UNITY_ANDROID
        ad.initAdmob("ca-app-pub-3205824340966339/8688885600", "");
        #else
        ad.initAdmob("ca-app-pub-3205824340966339/2642352006", "");
        #endif

        //ad.setTesting(true);

        ShowBanner();
    }

    public void ShowBanner()
    {
        ad.showBannerRelative(AdSize.SmartBanner, AdPosition.TOP_CENTER, 0);
    }
}
