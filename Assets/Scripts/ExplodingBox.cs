﻿using UnityEngine;
using System.Collections;

public class ExplodingBox : Box
{
    private float explosionTimer = 4f;
    private bool hasExploded = false;

    protected override void Update()
    {
        base.Update();
        explosionTimer -= Time.deltaTime;
        if (explosionTimer <= 0f)
            Explode();
    }

    public void Explode()
    {
        if (!hasExploded)
        {
            hasExploded = true;
            GameplayManager.Instance.Explode(ColumnIndex, GameplayManager.Instance.GetClosestBoxRow(fieldPosition));
        }
    }
}
