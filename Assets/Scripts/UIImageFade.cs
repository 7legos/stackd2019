﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIImageFade : MonoBehaviour
{
    [SerializeField]
    private bool startFaded;
    [SerializeField]
    private float initWaitTime;
    [SerializeField]
    private float fadeInTime;
    [SerializeField]
    private float fadeOutTime;
    [SerializeField]
    private float fadeInStayTime;
    [SerializeField]
    private float fadeOutStayTime;

    private Image myImage;
    private float alpha;
    private bool isFadingIn = true;
    private float lerper = 1f;
    private bool isFinalFade = false;

    void Awake()
    {
        myImage = GetComponent<Image>();
        if (startFaded)
        {
            alpha = 0f;
            lerper = 0f - initWaitTime;
            isFadingIn = true;
        }
        else
        {
            lerper = 1f + initWaitTime;
            alpha = 1f;
        }
    }

    void Update()
    {
        if(isFadingIn && !isFinalFade)
        {
            lerper += Time.deltaTime/fadeInTime;

            if (lerper > 1f + fadeInStayTime/2f)
                isFadingIn = false;
        } else
        {
            lerper -= Time.deltaTime/fadeOutTime;

            if (lerper < 0f - fadeOutStayTime / 2f)
            {
                isFadingIn = true;
                if (isFinalFade)
                    Destroy(this.gameObject);
            }
        }

        alpha = Mathf.Lerp(0f, 1f, lerper);
        myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, alpha);
    }

    public void FadeOut()
    {
        isFinalFade = true;
    }
}
