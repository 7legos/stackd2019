﻿using UnityEngine;
using System.Collections;

public abstract class FieldObject : MonoBehaviour
{
    [SerializeField]
    private Vector2 position;

    private float lastOffset = 0f;

    public Vector2 fieldPosition { get { return position; } set { position = value; CalculatePosition(); } }

    protected virtual void Awake()
    {
        fieldPosition = transform.position;
    }

    protected virtual void Update()
    {
        CalculatePosition();
        lastOffset = GameplayManager.Instance.CurrentOffset;
    }

    private void CalculatePosition()
    {
        if (GameplayManager.Instance != null)
        {
            if (GameplayManager.Instance.CurrentOffset < lastOffset)
            {
                position += (Vector2.down * lastOffset);
                OnOffset(lastOffset);
            }
            if (transform != null)
                transform.position = position - new Vector2(0f, GameplayManager.Instance.CurrentOffset);
        }
    }

    protected virtual void OnOffset(float lastOffset)
    {
    }
}