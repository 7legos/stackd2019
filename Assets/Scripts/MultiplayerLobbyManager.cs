﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiplayerLobbyManager : MonoBehaviour
{
    public static MultiplayerLobbyManager Instance;

    [SerializeField]
    private PlayerPanel playerPanelPrefab, firstPlayerPanel;

    [SerializeField]
    private Transform playerPanelParent;

    [SerializeField]
    private GameObject startButton;

    private List<PlayerPanel> playerPanels;

    private void Awake()
    {
        Instance = this;
        playerPanels = new List<PlayerPanel>();
        playerPanels.Add(firstPlayerPanel);
        MultiplayerInputManager.Instance.EnterLobby();
    }

    private void Update()
    {
        MultiplayerInputManager.Instance.CheckForJoiningPlayers();
        CheckAllPlayersReady();
    }

    private void CheckAllPlayersReady()
    {
        bool ready = true;
        foreach (PlayerPanel playerPanel in playerPanels)
        {
            if (playerPanels.Count < 3)
            {
                ready = false;
                break;
            }

            if ((!playerPanel.IsReady() && !playerPanel.IsWaitingForPlayer()))
            {
                ready = false;
                break;
            }
        }
        startButton.SetActive(ready && playerPanels.Count > 1);

        if (Input.GetKeyDown(KeyCode.Return) && ready)
            GameplayManager.Instance.PressStartMultiplayerMatch();
    }

    public void AddPlayer(int playerIndex)
    {
        PlayerPanel panel = playerPanels[playerIndex];
        panel.AttachPlayer(playerIndex);

        if (playerPanels.Count < 4)
        {
            PlayerPanel playerPanel = Instantiate(playerPanelPrefab, playerPanelParent, false);

            playerPanels.Add(playerPanel);
        }
    }
}