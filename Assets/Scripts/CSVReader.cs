﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class CSVReader
{
    public static List<string> GetColumn(string fileName, int columnIndex, int minRowIndex, int maxRowIndex)
    {
        List<string> columnList = new List<string>();
        columnList.Capacity = 50;
        TextAsset file = Resources.Load<TextAsset>(fileName);
        int readingRowNumberIndex;
        string line;
        List<StringBuilder> lineContents;
        using (var reader = new StreamReader(new MemoryStream(file.bytes)))
        {
            readingRowNumberIndex = 0;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();

                if (readingRowNumberIndex < minRowIndex)
                {
                    readingRowNumberIndex++;
                    continue;
                }
                if (readingRowNumberIndex > maxRowIndex)
                {
                    break;
                }

                lineContents = SeperateLineContents(line);
                columnList.Add(lineContents[columnIndex].ToString());
                readingRowNumberIndex++;
            }
        }

        return columnList;
    }

    public static List<string>[] GetMultipleColumns(string fileName, int[] columnIndexes, int minRowIndex, int maxRowIndex)
    {
        List<string>[] multipleColumnList = new List<string>[columnIndexes.Length];
        TextAsset file = Resources.Load<TextAsset>(fileName);
        string line;
        List<StringBuilder> lineContents;

        for (int j = 0; j < multipleColumnList.Length; j++)
        {
            multipleColumnList[j] = new List<string>();
        }

        using (var reader = new StreamReader(new MemoryStream(file.bytes)))
        {
            string[] allLines = reader.ReadToEnd().Split('\n');
            for (int i = minRowIndex; i <= maxRowIndex; i++)
            {
                line = allLines[i];
                lineContents = SeperateLineContents(line);

                for (int j = 0; j < multipleColumnList.Length; j++)
                {
                    multipleColumnList[j].Add(lineContents[columnIndexes[j]].ToString());
                }
            }
        }

        return multipleColumnList;
    }

    public static List<string>[] GetMultipleRows(string fileName, int minRowIndex, int maxRowIndex)
    {
        List<string>[] multipleRowList;
        //TextAsset file = Resources.Load<TextAsset>(fileName);
        //if (file == null)
        //    return null;

        string line;
        List<StringBuilder> lineContents;

        using (var reader = new StreamReader(File.Open(fileName, FileMode.Open)))
        {
            string[] allLines = reader.ReadToEnd().Split('\n');
            // string[] allLines = file.text.Split('\n');
            multipleRowList = new List<string>[allLines.Length + 1];
            maxRowIndex = Mathf.Min(maxRowIndex, allLines.Length - 1);
            for (int j = 0; j < multipleRowList.Length; j++)
            {
                multipleRowList[j] = new List<string>();
            }
            for (int i = minRowIndex; i <= maxRowIndex; i++)
            {
                line = allLines[i];
                lineContents = SeperateLineContents(line);

                List<string> rowList = new List<string>();
                for (int j = 0; j < lineContents.Count; j++)
                {
                    rowList.Add(lineContents[j].ToString());
                }
                multipleRowList[i - minRowIndex] = rowList;
                // }
            }

            return multipleRowList;
        }
    }

    private static List<StringBuilder> SeperateLineContents(string line)
    {
        List<StringBuilder> lineContents = new List<StringBuilder>();
        lineContents.Capacity = line.Length;
        int charIndex = 0;
        StringBuilder content = new StringBuilder();
        content.Capacity = line.Length;
        bool insideQuotes = false;
        char currentChar;
        while (charIndex < line.Length)
        {
            currentChar = line[charIndex];
            if (currentChar != '"')
            {
                if (currentChar != ',') //if the char is not a comma, add it to current content
                    content.Append(currentChar);
                else if (currentChar == ',')
                {
                    if (!insideQuotes)//if we reach a comma and we are not inside quotes, add what we read to the string list
                    {
                        lineContents.Add(content);
                        content = new StringBuilder();
                    }
                    else //else if we are inside quotes and reach a comma, it is a real comma and we add it to our string
                    {
                        content.Append(currentChar);
                    }
                }
            }
            else //if we reach quotes
            {
                if (!insideQuotes) //if we were not inside quotes before
                {
                    insideQuotes = true;
                }
                else //if we were already inside quotes
                {
                    insideQuotes = false;
                }
            }

            charIndex++;
        }
        lineContents.Add(content);

        return lineContents;
    }
}