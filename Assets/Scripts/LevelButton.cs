﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField]
    private Text myText;

    [SerializeField]
    private Button myButton;

    private int myLevel;
    private float checkTimer = 0f;

    private void Awake()
    {
        myLevel = transform.GetSiblingIndex() + 1;
        myText.text = myLevel.ToString();
        CheckForLevelFile();
        checkTimer = Random.Range(0f, 1f);
    }

    public void PressButton()
    {
        GameplayManager.Instance.PressLevelButton(myLevel);
    }

    private void Update()
    {
        checkTimer -= Time.deltaTime;
        if (checkTimer < 0f)
        {
            checkTimer = 1f;
            CheckForLevelFile();
        }
    }

    public void CheckForLevelFile()
    {
        myButton.interactable = ResourceManager.DoesLevelFileExist(myLevel);
    }
}