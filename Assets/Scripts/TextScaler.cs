﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextScaler : MonoBehaviour {
    [SerializeField]
    private float parentImagePercentage;

	// Use this for initialization
	void Start () {
        Debug.Log(transform.parent.GetComponent<RectTransform>().rect.height);
        GetComponent<Text>().fontSize = (int) (transform.parent.GetComponent<RectTransform>().rect.height * (parentImagePercentage / 100f));
	}
}
