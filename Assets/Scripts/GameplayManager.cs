﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Diagnostics;

//using Facebook.Unity;
//using UnityEngine.Advertisements;

[System.Serializable]
public enum GameState
{
    MenuWithFallingBlocks,
    Play,
    PlaySetup,
    Boost,
    GameOver
}

[System.Serializable]
public struct BoxSpawn
{
    public Box box;
    public int spawnWeight;
}

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance;

    public const int MAX_COLUMNS = 35;
    public const int MAX_ROWS = 20;
    public const int MAX_DIFFICULTY = 30; //difficulty is 0-100 scale of how fast boxes will spawn, currently the max of 30 is reached at a score of 3300
    public const float SPAWN_HEIGHT = 24f;
    public const float BOX_START_VELOCITY = 4f;
    public const float BOX_VELOCITY_DELTA = 1f;
    public const float BOX_VELOCITY_DELTA_DELTA = 0.2f;
    public const float BEAM_START_VELOCITY = 40f;
    public const float BEAM_VELOCITY_DELTA = 50f;
    public const float EPSILON = 0.00001f;
    public const int PULL_ROW_INDEX = 4;
    public const float OFFSET_DIVIDER = 0.1f;
    public const float OFFSET_DIVIDER_SPEED = 0.03f; //used to speed things up when 2 rows need to be offsetted
    public const int MAX_SPAWN_COLUMN_HEIGHT = 11; //dont spawn a box if a column is this high
    public const int SPAWN_SHARD_COUNT = 5;
    public const float COLUMN_TIMER = 1.3f; //Time before the same column can spawn again
    public const float RED_COLUMN_TIMER = 4f; //Time before column can spawn after a red block is spawned
    public const float PLAYER_HORIZONTAL_SPEED = 0.11f;
    public const float PLAYER_VELOCITY_DELTA = 28f;
    public const float SETUP_STEP_TIME = 0.055f; //Time between blocks spawning in setup
    public const float SETUP_STEP_TIME_BEFORE_PLAYER = 0.25f; //Time before spawning player
    public const int SETUP_STEPS = 24;
    public const float PLAYER_BOUNDS = 0.28f;
    public const int PARTICLE_COUNT_PER_BOX = 25;
    public const int PARTICLE_COUNT_PLAYER = 300;
    public const int MAX_DISPARITY = 4;
    public const int SPAWN_BOX_ON_PLAYER = 4;
    public const int DEATH_AD_SCORE = 6;

    [SerializeField]
    private int minBoxSpawnTime;

    [SerializeField]
    private int maxBoxSpawnTime;

    [SerializeField]
    private Player playerPrefab;

    [SerializeField]
    private Box[] initBoxes;

    [SerializeField]
    private BoxSpawn[] boxSpawns;

    [SerializeField]
    private GameObject pauseMenu;

    [SerializeField]
    private GameObject pauseButton;

    [SerializeField]
    private GameObject scoreUI, mainMenu, levelSelectionMenu, multiplayerMenu, retryLevelMenuButton;

    [SerializeField]
    private Text[] textUI;

    [SerializeField]
    private Image[] imageUI;

    [SerializeField]
    private Image boostButton;

    [SerializeField]
    private Image handTapLeft, handTapRight;

    [SerializeField]
    private Image soundUI;

    [SerializeField]
    private ScoreManager scoreManager;

    [SerializeField]
    private EventSystem eventSystem;

    [SerializeField]
    private Toggle invincibleCheckBox;

    public BoxSpawn[] BoxSpawns { get { return boxSpawns; } }
    public List<Box>[] Boxes { get { return boxes; } }
    public bool IsPaused { get { return isPaused; } }

    public float CurrentOffset = 0f;
    public Player Player;
    private List<Player> multiPlayers;

    private float currentOffsetDivider = OFFSET_DIVIDER;

    private ParticlePoolManager particlePoolManager;
    private List<Box>[] boxes;
    private float[] columnSpawnTimers;
    private float setupTimer = SETUP_STEP_TIME;
    private int setupStep = 0; //block spawn state during setup
    private GameState gameState;
    private float spawnBoxTimer;
    private bool isOffsetting = false;
    private bool isPaused = false;
    private int playerBlockCounter = 0; //When this is SPAWN_BOX_ON_PLAYER, spawn a box at the player's column
    private float orangeBoxOverrideTimer = 0f; //Orange blocks cant spawn when this is >0f
    private GameObject currentSelectedUI;

    private List<string>[] loadedLevelInfo; //array of box lists
    private List<string>[] loadedMusicLevelInfo; //array of box lists
    private int currentLoadedRowIndex = 0;
    private float loadedLevelRowTimer = 0f;
    private float nextAbsoluteRowSpawnTime;
    private string randomGenSeed;
    private int numberOfRandomBoxesToSpawn, numberOfRandomBoxes2ToSpawn;
    private int randomGenBoxWeight1, randomGenBoxWeight2, randomGenBoxWeight3, randomGenBoxWeight4, randomGenBoxWeight5;
    private int randomGenBox2Weight1, randomGenBox2Weight2, randomGenBox2Weight3, randomGenBox2Weight4, randomGenBox2Weight5;
    private System.Random fileLoaderRandomBox, fileLoaderRandomBox2;
    private bool invincibleMode;
    private Stopwatch stopWatch;
    private float startLevelTime;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            UnityEngine.Debug.LogError("More than 1 GameplayManager detected");

        // Time.timeScale = 0.5f;
        //scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        //eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();

        // #if UNITY_ANDROID Advertisement.Initialize("1317774", true); #else
        // Advertisement.Initialize("1317773", true); #endif

        boxes = new List<Box>[MAX_COLUMNS];
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            boxes[i] = new List<Box>();
        }
        columnSpawnTimers = new float[MAX_COLUMNS];
        // ToggleMusic(); ToggleMusic();
        AudioManager.Instance.SetUIButton(soundUI, true);
    }

    private void Start()
    {
        // SetupGame();
        particlePoolManager = ParticlePoolManager.Instance;
        // StartCoroutine(FadeIn());
    }

    public void PressPlayButton()
    {
        CloseMainMenu();
        PersistantSettings.Instance.SetMultiplayerMode(false);
        PersistantSettings.Instance.SetLastPlayerChosenGameMode(GameMode.Endless);
        SetupGame(GameMode.Endless);
    }

    public void PressMultiplayerButton()
    {
        OpenMultiplayerMenu();
    }

    public void PressStartMultiplayerMatch()
    {
        CloseMultiplayerMenu();
        CloseMainMenu();
        PersistantSettings.Instance.SetLastPlayerChosenGameMode(GameMode.Endless);
        PersistantSettings.Instance.SetMultiplayerMode(true);
        SetupGame(GameMode.Endless);
    }

    public void PressLevelButton(int levelNumber)
    {
        CloseMainMenu();
        PersistantSettings.Instance.SetMultiplayerMode(false);
        PersistantSettings.Instance.SetLastPlayerChosenGameMode(GameMode.LevelFromFile);
        SetupGame(GameMode.LevelFromFile, levelNumber);
    }

    public void PressLevelSelectionButton()
    {
        OpenLevelSelectionMenu();
    }

    private void SetupGame(GameMode gameMode, int levelNumber = 0)
    {
        DestroyMenuBoxes(false);
        boxes = new List<Box>[MAX_COLUMNS];
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            boxes[i] = new List<Box>();
        }
        columnSpawnTimers = new float[MAX_COLUMNS];
        PersistantSettings.Instance.SetGameMode(gameMode);
        PlayerPrefs.SetInt("currentScore", 0);
        gameState = GameState.PlaySetup;
        scoreManager.Score = 0;
        currentLoadedRowIndex = 0;
        CurrentOffset = 0;
        isOffsetting = false;
        setupStep = 0;
        setupTimer = 0.5f;
        spawnBoxTimer = 1f;
        int rowTimer;
        switch (PersistantSettings.Instance.GetGameMode())
        {
            case GameMode.Endless:
                break;

            case GameMode.LevelFromFile:
                if (levelNumber > 0)
                    ResourceManager.LoadLevel(levelNumber);
                else
                    ResourceManager.ReloadLevel();
                loadedLevelInfo = ResourceManager.GetLevelRows();
                int.TryParse(loadedLevelInfo[0][0], out rowTimer);
                loadedLevelRowTimer = rowTimer / 1000f;
                break;

            case GameMode.MusicLevelFromFile:
                ResourceManager.LoadMusicLevel();
                loadedMusicLevelInfo = ResourceManager.GetLevelMusicRows();
                int.TryParse(loadedMusicLevelInfo[0][3], out rowTimer);
                // loadedLevelRowTimer = rowTimer / 1000f;
                nextAbsoluteRowSpawnTime = rowTimer / 1000f;
                break;
        }
    }

    public void StartGame()
    {
        if (gameState == GameState.PlaySetup || gameState == GameState.Boost)
        {
            gameState = GameState.Play;
            StartCoroutine(FadeBoostButton());
            FadeHandTaps();
            if (PersistantSettings.Instance.IsGameMode(GameMode.MusicLevelFromFile))
            {
                startLevelTime = Time.time;
            }
        }
    }

    public void StartMusicLevel()
    {
        CloseMainMenu();
        PersistantSettings.Instance.SetGameMode(GameMode.MusicLevelFromFile);
        PersistantSettings.Instance.SetMultiplayerMode(false);
        PersistantSettings.Instance.SetLastPlayerChosenGameMode(GameMode.MusicLevelFromFile);
        SetupGame(GameMode.MusicLevelFromFile);
    }

    private void CloseMainMenu()
    {
        CloseLevelSelectionMenu();
        retryLevelMenuButton.SetActive(true);
        mainMenu.SetActive(false);
    }

    private void OpenMainMenu()
    {
        PersistantSettings.Instance.SetGameMode(GameMode.Endless);
        gameState = GameState.MenuWithFallingBlocks;
        mainMenu.SetActive(true);
    }

    public void OpenLevelSelectionMenu()
    {
        levelSelectionMenu.SetActive(true);
    }

    public void OpenMultiplayerMenu()
    {
        multiplayerMenu.SetActive(true);
    }

    public void CloseMultiplayerMenu()
    {
        multiplayerMenu.SetActive(false);
    }

    public void CloseLevelSelectionMenu()
    {
        levelSelectionMenu.SetActive(false);
    }

    public void RestartLevel()
    {
        if (Player != null)
            Player.Die(Player.fieldPosition, false, true);
        else
            OnDeath(null, false);

        CloseMainMenu();
        Unpause();
        SetupGame(PersistantSettings.Instance.GetLastPlayerChosenGameMode(), -1);
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    public GameState GetGameState()
    {
        return gameState;
    }

    //called by every box on awake() to get added to the list above
    public void RegisterBox(Box box, int columnIndex)
    {
        boxes[columnIndex].Add(box);
    }

    public void UnregisterBox(Box box, int columnIndex)
    {
        boxes[columnIndex].Remove(box);
    }

    //Tell other boxes above in the same column a box has just landed
    public void ColumnUpOffset(int columnIndex, Box callingBox)
    {
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            if (boxes[columnIndex][i] != null)
            {
                if (boxes[columnIndex][i] != callingBox && boxes[columnIndex][i].fieldPosition.y > callingBox.fieldPosition.y)
                {
                    boxes[columnIndex][i].UpOffset();
                }
            }
        }
    }

    public bool IsUISelected()
    {
        //currentSelectedUI
        if (Input.touches.Length > 0 && eventSystem.currentSelectedGameObject)
            return true;

        return false;
    }

    //check the tile and tile above it for blocks that could be falling
    public bool IsSpawnSpaceFree(int columnIndex, int rowIndex)
    {
        if (columnIndex < 0 || rowIndex < 0)
            return false;

        if (columnIndex >= MAX_COLUMNS || rowIndex >= MAX_ROWS)
            return false;

        int boxRow;
        int boxRow2;
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            boxRow = Mathf.RoundToInt(boxes[columnIndex][i].fieldPosition.y);
            if (boxRow == rowIndex)
                return false;

            //When we are in the air we effectively take up 2 spaces
            if (boxes[columnIndex][i].InAir)
            {
                boxRow2 = boxes[columnIndex][i].fieldPosition.y < boxRow ? boxRow - 1 : boxRow + 1;
                if (boxRow2 == rowIndex && boxes[columnIndex][i].IsActivated)
                    return false;
            }
        }

        return true;
    }

    public void MultiPlayerStep(float scale)
    {
        if (multiPlayers != null && multiPlayers.Count > 0)
        {
            for (int i = 0; i < multiPlayers.Count; i++)
            {
                if (multiPlayers[i] != null)
                    multiPlayers[i].Step(scale);
            }
        }
    }

    private void Update()
    {
        SetInvincibleMode();

        if (gameState == GameState.Play || gameState == GameState.MenuWithFallingBlocks)
        {
            PlayStep();
            if (gameState == GameState.MenuWithFallingBlocks)
            {
                scoreManager.Score = 0; //dont score on menu boxes
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
            RestartLevel();

        if (gameState == GameState.Play || gameState == GameState.Boost || gameState == GameState.MenuWithFallingBlocks)
        {
            OffsetStep();
        }

        if (gameState == GameState.PlaySetup && setupStep < SETUP_STEPS)
        {
            setupTimer -= Time.deltaTime;

            if (setupTimer <= 0f)
            {
                if (setupStep < (MAX_COLUMNS % 2 == 0 ? MAX_COLUMNS / 2 : MAX_COLUMNS / 2 + 1))
                {
                    if (MAX_COLUMNS % 2 == 0) //even total column count spawns init boxes slightly differently
                    {
                        int columnIndex1 = MAX_COLUMNS / 2 + setupStep;
                        int columnIndex2 = MAX_COLUMNS / 2 - setupStep - 1;
                        SpawnInitBox(columnIndex1, (int)(columnIndex1 / (float)MAX_COLUMNS * 5f));
                        SpawnInitBox(columnIndex2, (int)(columnIndex2 / (float)MAX_COLUMNS * 5f));
                    }
                    else
                    {
                        if (setupStep == 0)
                        {
                            int columnIndex = MAX_COLUMNS / 2;
                            SpawnInitBox(columnIndex, (int)(columnIndex / (float)MAX_COLUMNS * 5f));
                        }
                        else
                        {
                            int columnIndex1 = MAX_COLUMNS / 2 + setupStep;
                            int columnIndex2 = MAX_COLUMNS / 2 - setupStep;
                            SpawnInitBox(columnIndex1, (int)(columnIndex1 / (float)MAX_COLUMNS * 5f));
                            SpawnInitBox(columnIndex2, (int)(columnIndex2 / (float)MAX_COLUMNS * 5f));
                        }
                    }
                }

                setupStep++;
                setupTimer = SETUP_STEP_TIME;
                if (setupStep == SETUP_STEPS)
                {
                    if (PersistantSettings.Instance.IsMultiplayerMode())
                        SpawnMultiPlayers();
                    else
                        SpawnPlayer(17f);
                    // StartCoroutine(FadeInBoostButton());
                }
                else if (setupStep == SETUP_STEPS - 1)
                {
                    setupTimer = SETUP_STEP_TIME_BEFORE_PLAYER;
                }
            }
        }
    }

    private void OffsetStep()
    {
        if (GetRowSize(PULL_ROW_INDEX + 1) >= MAX_COLUMNS)
        {
            currentOffsetDivider = OFFSET_DIVIDER_SPEED;
        }
        else
        {
            currentOffsetDivider = OFFSET_DIVIDER;
        }

        if (GetRowSize(PULL_ROW_INDEX) >= MAX_COLUMNS && !isOffsetting) //Second to last row is full, start to pull all boxes downwards
        {
            for (int i = 0; i < MAX_COLUMNS; i++)
            {
                for (int j = 0; j < boxes[i].Count; j++)
                {
                    if (!boxes[i][j].InAir && Mathf.RoundToInt(boxes[i][j].fieldPosition.y) == PULL_ROW_INDEX && boxes[i][j] is ExplodingBox)
                    {
                        boxes[i][j].ChangeBoxType(0);
                    }
                }
            }
            isOffsetting = true;
        }
        OffsetBoxes();
    }

    private void PlayStep()
    {
        if (PersistantSettings.Instance.IsGameMode(GameMode.Endless))
        {
            spawnBoxTimer -= Time.deltaTime;
            orangeBoxOverrideTimer -= Time.deltaTime;
            for (int i = 0; i < MAX_COLUMNS; i++)
            {
                columnSpawnTimers[i] -= Time.deltaTime;
            }

            if (spawnBoxTimer <= 0f)
            {
                int randomColumn = Random.Range(0, MAX_COLUMNS);
                int columnSize = GetColumnSize(randomColumn);
                int randomBoxIndex = GetRandomBoxIndex();

                if (!BoxCheck(randomColumn, columnSize, randomBoxIndex))
                {
                    spawnBoxTimer = 0f;
                }
                else
                {
                    //Make sure we spawn a box on the shortest column if another column is too high to even things out
                    if (GetDisparity() >= MAX_DISPARITY)
                    {
                        randomColumn = GetShortestColumnIndex(Random.Range(0, 100) < 50 ? true : false);
                        columnSize = GetColumnSize(randomColumn);
                        if (randomBoxIndex == 4)
                            randomBoxIndex = 0;
                    }

                    if (columnSize < MAX_SPAWN_COLUMN_HEIGHT && columnSpawnTimers[randomColumn] <= 0f)
                    {
                        int playerColumn = GetPlayerColumnIndex();
                        playerBlockCounter++;
                        int difficulty = Mathf.Clamp((scoreManager.Score - 300) / 100, 0, MAX_DIFFICULTY);
                        spawnBoxTimer = Random.Range(minBoxSpawnTime / 1000f, maxBoxSpawnTime / 1000f);
                        spawnBoxTimer *= ((100 - difficulty) / 100f);
                        if (playerBlockCounter == SPAWN_BOX_ON_PLAYER && columnSpawnTimers[playerColumn] <= 0f) // Override and spawn a block on player
                        {
                            SpawnBox(playerColumn, 0);
                            playerBlockCounter = 0;
                        }
                        else
                        {
                            SpawnBox(randomColumn, randomBoxIndex);
                        }
                    }
                    else
                    {
                        spawnBoxTimer = 0f;
                    }
                }
            }
        }
        else if (PersistantSettings.Instance.IsGameMode(GameMode.LevelFromFile)) //box spawn logic for predefined levels
        {
            loadedLevelRowTimer -= Time.deltaTime;
            if (loadedLevelRowTimer < 0f && currentLoadedRowIndex < loadedLevelInfo.Length)
            {
                bool parsingSeedRow = false;
                for (int i = 0; i < loadedLevelInfo[currentLoadedRowIndex].Count; i++)
                {
                    if (!(loadedLevelInfo[currentLoadedRowIndex][i] == ""))
                    {
                        if (parsingSeedRow) //we saw an X in the first column of our row, dont spawn
                        {
                            switch (i)
                            {
                                case 2:
                                    int randomGenSeed;
                                    bool parsed = int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenSeed);
                                    if (parsed)
                                        fileLoaderRandomBox = new System.Random(randomGenSeed);
                                    //else
                                    //    SpawnRandomGenBox();
                                    break;

                                case 3:
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenBoxWeight1);
                                    break;

                                case 4:
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenBoxWeight2);
                                    break;

                                case 5:
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenBoxWeight3);
                                    break;

                                case 6:
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenBoxWeight4);
                                    break;

                                case 7:
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out randomGenBoxWeight5);
                                    break;

                                case 8:
                                    int numberOfBoxes;
                                    int.TryParse(loadedLevelInfo[currentLoadedRowIndex][i], out numberOfBoxes);
                                    for (int j = 0; j < numberOfBoxes; j++)
                                    {
                                        // SpawnRandomGenBox(null);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            string boxType = loadedLevelInfo[currentLoadedRowIndex][i].Substring(0, 1);
                            switch (boxType)
                            {
                                case "X":
                                case "x":
                                    if (i == 1)
                                        parsingSeedRow = true;
                                    break;

                                case "B":
                                case "b":
                                    if (!parsingSeedRow)
                                        SpawnBoxNoTimer(i - 1, 0);
                                    break;

                                case "O":
                                case "o":
                                    if (!parsingSeedRow)
                                        SpawnBoxNoTimer(i - 1, 1);
                                    break;

                                case "P":
                                case "p":
                                    if (!parsingSeedRow)
                                        SpawnBoxNoTimer(i - 1, 2);
                                    break;

                                case "G":
                                case "g":
                                    if (!parsingSeedRow)
                                        SpawnBoxNoTimer(i - 1, 3);
                                    break;

                                case "R":
                                case "r":
                                    if (!parsingSeedRow)
                                        SpawnBoxNoTimer(i - 1, 4);
                                    break;
                            }
                        }
                    }
                }

                currentLoadedRowIndex++;
                if (currentLoadedRowIndex < loadedLevelInfo.Length)
                {
                    int rowTimer;
                    if (loadedLevelInfo[currentLoadedRowIndex].Count > 0 && int.TryParse(loadedLevelInfo[currentLoadedRowIndex][0], out rowTimer))
                        loadedLevelRowTimer = rowTimer / 1000f;
                }
            }
        }
        else if (PersistantSettings.Instance.IsGameMode(GameMode.MusicLevelFromFile)) //box spawn logic for predefined levels
        {
            // loadedLevelRowTimer -= Time.deltaTime;

            if (Time.time - startLevelTime > nextAbsoluteRowSpawnTime)
            {
                LevelRowData levelRowData = new LevelRowData();
                levelRowData.boxNames = new string[MAX_COLUMNS];
                levelRowData.boxWeights = new int[5];
                levelRowData.boxWeights2 = new int[5];

                for (int i = 3; i < loadedMusicLevelInfo[currentLoadedRowIndex].Count; i++)
                {
                    //if (i == 3) //row timer
                    //{
                    //    int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out levelRowData.rowTimer);
                    //}
                    if (i > 3 && i < 39)//columns 1-35
                    {
                        if (loadedMusicLevelInfo[currentLoadedRowIndex][i] != "")
                        {
                            string boxType = loadedMusicLevelInfo[currentLoadedRowIndex][i].Substring(0, 1);
                            levelRowData.boxNames[i - 4] = boxType;
                        }
                        else
                        {
                            levelRowData.boxNames[i - 4] = "";
                        }
                    }
                    else if (i == 39) //seed
                    {
                        if (loadedMusicLevelInfo[currentLoadedRowIndex][i] != "")
                        {
                            int randomGenSeed;
                            bool parsed = int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out randomGenSeed);
                            if (parsed)
                            {
                                fileLoaderRandomBox = new System.Random(randomGenSeed);
                                levelRowData.weights1Changed = true;
                            }
                        }
                    }
                    else if (i > 39 && i < 45)//box weights
                    {
                        int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out levelRowData.boxWeights[i - 40]);
                    }
                    else if (i == 45) //number of boxes to spawn within the x's
                    {
                        int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out levelRowData.numberOfRandomBoxesToSpawn);
                    }
                    else if (i == 46) //seed 2
                    {
                        if (loadedMusicLevelInfo[currentLoadedRowIndex][i] != "")
                        {
                            int randomGenSeed;
                            bool parsed = int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out randomGenSeed);
                            if (parsed)
                            {
                                fileLoaderRandomBox2 = new System.Random(randomGenSeed);
                                levelRowData.weights2Changed = true;
                            }
                        }
                    }
                    else if (i > 46 && i < 52)//box weights
                    {
                        int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out levelRowData.boxWeights2[i - 47]);
                    }
                    else if (i == 52) //number of boxes to spawn within the z's
                    {
                        int.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][i], out levelRowData.numberOfRandomBoxes2ToSpawn);
                    }
                    else if (i == 53)
                    {
                        if (levelRowData.note == null)
                        {
                            levelRowData.note = new MusicNoteData
                            {
                                noteName = loadedMusicLevelInfo[currentLoadedRowIndex][i],
                                fadeOutTime = 0
                            };
                        }
                    }
                }
                SpawnLevelRow(levelRowData);//spawn everything

                currentLoadedRowIndex++;
                if (currentLoadedRowIndex < loadedMusicLevelInfo.Length)
                {
                    float rowTimer = float.MaxValue;
                    if (loadedMusicLevelInfo[currentLoadedRowIndex].Count > 1)
                        float.TryParse(loadedMusicLevelInfo[currentLoadedRowIndex][2], out rowTimer);
                    // loadedLevelRowTimer = rowTimer / 1000f;

                    nextAbsoluteRowSpawnTime = rowTimer / 1000f;
                }
                else
                {
                    nextAbsoluteRowSpawnTime = float.MaxValue;
                }
            }
        }
    }

    private void SpawnLevelRow(LevelRowData levelRowData)
    {
        List<int> possibleRandomColumnIndexSpawns = new List<int>(); //index of where every x is
        List<int> possibleRandomColumnIndexSpawns2 = new List<int>(); //index of where every z is

        for (int i = 0; i < levelRowData.boxNames.Length; i++)
        {
            switch (levelRowData.boxNames[i])
            {
                case "X":
                case "x":
                    possibleRandomColumnIndexSpawns.Add(i);
                    break;

                case "Z":
                case "z":
                    possibleRandomColumnIndexSpawns2.Add(i);
                    break;

                case "B":
                case "b":
                    SpawnBoxNoTimer(i, 0);
                    break;

                case "O":
                case "o":
                    SpawnBoxNoTimer(i, 1);
                    break;

                case "P":
                case "p":
                    SpawnBoxNoTimer(i, 2);
                    break;

                case "G":
                case "g":
                    SpawnBoxNoTimer(i, 3);
                    break;

                case "R":
                case "r":
                    SpawnBoxNoTimer(i, 4);
                    break;
            }
        }

        //set weights
        if (levelRowData.weights1Changed)
        {
            numberOfRandomBoxesToSpawn = levelRowData.numberOfRandomBoxesToSpawn;
            randomGenBoxWeight1 = levelRowData.boxWeights[0];
            randomGenBoxWeight2 = levelRowData.boxWeights[1];
            randomGenBoxWeight3 = levelRowData.boxWeights[2];
            randomGenBoxWeight4 = levelRowData.boxWeights[3];
            randomGenBoxWeight5 = levelRowData.boxWeights[4];
        }

        if (levelRowData.weights2Changed)
        {
            numberOfRandomBoxes2ToSpawn = levelRowData.numberOfRandomBoxes2ToSpawn;
            randomGenBox2Weight1 = levelRowData.boxWeights2[0];
            randomGenBox2Weight2 = levelRowData.boxWeights2[1];
            randomGenBox2Weight3 = levelRowData.boxWeights2[2];
            randomGenBox2Weight4 = levelRowData.boxWeights2[3];
            randomGenBox2Weight5 = levelRowData.boxWeights2[4];
        }

        if (possibleRandomColumnIndexSpawns.Count > 0 && numberOfRandomBoxesToSpawn > 0)
        {
            SpawnRandomGenBox(possibleRandomColumnIndexSpawns, numberOfRandomBoxesToSpawn, 1);
        }

        if (possibleRandomColumnIndexSpawns2.Count > 0 && numberOfRandomBoxes2ToSpawn > 0)
        {
            SpawnRandomGenBox(possibleRandomColumnIndexSpawns2, numberOfRandomBoxes2ToSpawn, 2);
        }

        //play the notes
        //   AudioManager.Instance.PlayMusicNote(levelRowData.note);
    }

    public void SetInvincibleMode()
    {
        invincibleMode = invincibleCheckBox.isOn;
    }

    public bool GetInvincibleMode()
    {
        return invincibleMode;
    }

    public float GetClosestPlayerDistance(Vector2 testingPosition)
    {
        float closestDistance = float.MaxValue;
        for (int i = 0; i < multiPlayers.Count; i++)
        {
            float distance = Vector2.Distance(testingPosition, multiPlayers[i].transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
            }
        }
        return closestDistance;
    }

    public void StartBoost()
    {
        // print("StartBoost 1"); if (gameState == GameState.PlaySetup) { print("StartBoost 2");

        // if (Advertisement.IsReady("rewardedVideo")) { print("StartBoost 3"); var options = new
        // ShowOptions { resultCallback = RewardAdCallback }; Advertisement.Show("rewardedVideo",
        // options); }

        // }
    }

    // private void RewardAdCallback(ShowResult result) { print("RewardAdCallback 4");

    // if (result == ShowResult.Finished) { gameState = GameState.Boost;

    // PlayerPrefs.SetInt("deathsUntilAd", DEATH_AD_SCORE + 1); StartCoroutine(FadeBoostButton());
    // FadeHandTaps(); StartCoroutine(Boost()); } }

    private IEnumerator FadeInBoostButton()
    {
        float alpha = 0f;

        if (boostButton == null)
            yield break;

        while (alpha < 1f)
        {
            alpha += 0.05f;
            boostButton.color = new Color(boostButton.color.r, boostButton.color.g, boostButton.color.b, alpha);
            yield return new WaitForEndOfFrame();
        }

        boostButton.GetComponent<Button>().enabled = true;
    }

    private IEnumerator FadeBoostButton()
    {
        float alpha = 1f;

        if (boostButton == null)
            yield break;

        while (alpha > 0f)
        {
            alpha -= 0.05f;
            boostButton.color = new Color(boostButton.color.r, boostButton.color.g, boostButton.color.b, alpha);
            yield return new WaitForEndOfFrame();
        }

        Destroy(boostButton.gameObject);
    }

    private IEnumerator Boost()
    {
        int currentHighScore = PlayerPrefs.GetInt("highScore", 0);
        int boostScore = (int)Mathf.Max(50f, currentHighScore / 2 - (currentHighScore / 2 % 10));

        for (int i = 0; i < MAX_COLUMNS / 2; i++)
        {
            GetTopBox(4 - i).SpawnBoostBoxes(boostScore / 10);
            GetTopBox(5 + i).SpawnBoostBoxes(boostScore / 10);

            yield return new WaitForSeconds(0.06f);
        }
    }

    private void FadeHandTaps()
    {
        if (handTapLeft != null && handTapRight != null)
        {
            handTapLeft.GetComponent<UIImageFade>().FadeOut();
            handTapRight.GetComponent<UIImageFade>().FadeOut();
        }
    }

    public void ToggleMusic()
    {
        AudioManager.Instance.ToggleMusic(soundUI, true);
    }

    public void PressPause()
    {
        if (gameState == GameState.Play || gameState == GameState.PlaySetup || gameState == GameState.Boost)
        {
            isPaused = !isPaused;

            if (IsPaused)
            {
                Pause();
            }
            else
            {
                Unpause();
            }
        }
    }

    private void Pause()
    {
        pauseMenu.SetActive(true);
        pauseButton.SetActive(false);
        isPaused = true;
        // scoreUI.SetActive(false);
        Time.timeScale = 0.000001f;
    }

    private void Unpause()
    {
        pauseMenu.SetActive(false);
        pauseButton.SetActive(true);
        isPaused = false;
        // scoreUI.SetActive(true);
        Time.timeScale = 1f;
    }

    private int GetDisparity()
    {
        int shortestColumnHeight = MAX_ROWS;
        int tallestColumnHeight = 0;

        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            int columnSize = GetColumnSize(i);
            if (columnSize < shortestColumnHeight)
                shortestColumnHeight = columnSize;

            if (columnSize > tallestColumnHeight)
                tallestColumnHeight = columnSize;
        }

        return tallestColumnHeight - shortestColumnHeight;
    }

    public void PressMenu()
    {
        DestroyMenuBoxes(false);
        if (Player != null)
            Player.Die(Player.fieldPosition, true, true);
        else
            OnDeath(null, true);
        Unpause();
    }

    public void OnDeath(Player player, bool openMenu)
    {
        AudioManager.Instance.PlaySoundEffect(SoundEffect.PlayerDeath);

        // int deathAdScore = PlayerPrefs.GetInt("deathsUntilAd", DEATH_AD_SCORE);

        // if (scoreManager.Score >= 50) deathAdScore -= 2; else if (scoreManager.Score >=
        // 100) deathAdScore -= DEATH_AD_SCORE; else deathAdScore--;

        // PlayerPrefs.SetInt("deathsUntilAd", deathAdScore);

        // if (deathAdScore <= 0 && Advertisement.IsReady("video")) {
        // PlayerPrefs.SetInt("deathsUntilAd", DEATH_AD_SCORE); Advertisement.Show("video"); }
        if (PersistantSettings.Instance.IsMultiplayerMode())
        {
            multiPlayers.Remove(player);
        }

        if (!PersistantSettings.Instance.IsMultiplayerMode() || (PersistantSettings.Instance.IsMultiplayerMode() && NumberOfPlayersAlive() == 0))
        {
            TrySubmitHighScore();

            PlayerPrefs.SetInt("currentScore", scoreManager.Score);
            gameState = GameState.GameOver;
            StartCoroutine(MenuTransition(openMenu));
        }
    }

    public int NumberOfPlayersAlive()
    {
        int alivePlayerCount = 0;
        if (PersistantSettings.Instance.IsMultiplayerMode())
        {
            for (int i = 0; i < multiPlayers.Count; i++)
            {
                if (!multiPlayers[i].IsDead())
                {
                    alivePlayerCount++;
                    break;
                }
            }
        }
        else return 1; //single player

        return alivePlayerCount;
    }

    private void TrySubmitHighScore()
    {
        int currentHighScore = PlayerPrefs.GetInt("highScore", 0);
        int currentHighScoreFB = PlayerPrefs.GetInt("currentHighScoreFB", 0);

        //Local Storage
        if (scoreManager.Score > currentHighScore)
            PlayerPrefs.SetInt("highScore", scoreManager.Score);

        //Facebook Scores
        // if (FB.IsLoggedIn && currentHighScore > currentHighScoreFB)
        // {
        //     Dictionary<string, string> submitScore = new Dictionary<string, string>();
        //     submitScore.Add("score", currentHighScore.ToString());

        // FB.API("/me/scores", HttpMethod.POST, SubmitScoreCallbackFB, submitScore);
        // PlayerPrefs.SetInt("currentHighScoreFB", currentHighScore); }

        //Google Play Leaderboards
        // if (Social.localUser.authenticated)
        //     Social.ReportScore(PlayerPrefs.GetInt("highScore", 0), GooglePlayManager.LeaderboardID, SubmitScoreCallbackGP);
    }

    private void SubmitScoreCallbackGP(bool obj)
    {
    }

    // private void SubmitScoreCallbackFB(IGraphResult result) { }

    private IEnumerator MenuTransition(bool openMenuAtEnd)
    {
        DestroyMenuBoxes(openMenuAtEnd);
        if (openMenuAtEnd)
            yield return new WaitForSeconds(1.1f);

        //int highestRowCount = boxes[GetTallestColumnIndex(true)].Count;
        //int lastSoundEffectPlayK = int.MaxValue;
        //for (int i = highestRowCount; i >= 0; i--)
        //{
        //    for (int j = 0; j < MAX_COLUMNS; j++)
        //    {
        //        for (int k = boxes[j].Count - 1; k >= 0; k--)
        //        {
        //            if (boxes[j][k].fieldPosition.y >= i)
        //            {
        //                boxes[j][k].DestroyBox(false, false, true);
        //                if (k != lastSoundEffectPlayK)
        //                {
        //                    AudioManager.Instance.PlaySoundEffect(SoundEffect.EndBoxExplosion);
        //                    lastSoundEffectPlayK = k;
        //                }
        //            }
        //        }
        //    }
        //    yield return new WaitForSeconds(0.15f);
        //}
        // StartCoroutine(FadeOut());
        yield return new WaitForSeconds(0.01f);

        if (PersistantSettings.Instance.IsMultiplayerMode())
        {
            OpenMainMenu();
            OpenMultiplayerMenu();
        }
        else
        if (openMenuAtEnd)
            OpenMainMenu();
    }

    private void DestroyMenuBoxes(bool initDelay, float deltaDelay = 0.02f)
    {
        if (initDelay)
            StartCoroutine(DelayedClearBoxes(0.04f, deltaDelay));
        else
        {
            List<Box>[] boxesCache = (List<Box>[])boxes.Clone();
            int highestRowCount = boxesCache[GetTallestColumnIndex(true)].Count;
            int rowIterations = 0;
            bool destroyedBox = false;

            for (int i = highestRowCount; i >= 0; i--)
            {
                for (int j = 0; j < MAX_COLUMNS; j++)
                {
                    for (int k = boxesCache[j].Count - 1; k >= 0; k--)
                    {
                        if (boxesCache[j][k].fieldPosition.y >= i)
                        {
                            destroyedBox = true;
                            boxesCache[j][k].DestroyBox(false, true, false, false, rowIterations * deltaDelay);
                        }
                    }
                }
                if (destroyedBox)
                    rowIterations++;
                destroyedBox = false;
            }

            boxes = new List<Box>[MAX_COLUMNS];
            for (int i = 0; i < MAX_COLUMNS; i++)
            {
                boxes[i] = new List<Box>();
            }
            columnSpawnTimers = new float[MAX_COLUMNS];
        }
    }

    private IEnumerator DelayedClearBoxes(float delay, float deltaDelay)
    {
        List<Box>[] boxesCache = (List<Box>[])boxes.Clone();
        int highestRowCount = boxesCache[GetTallestColumnIndex(true)].Count;
        int rowIterations = 0;
        for (int i = highestRowCount; i >= 0; i--)
        {
            for (int j = 0; j < MAX_COLUMNS; j++)
            {
                for (int k = boxesCache[j].Count - 1; k >= 0; k--)
                {
                    if (boxesCache[j][k].fieldPosition.y >= i)
                    {
                        boxesCache[j][k].StopSpawns();
                    }
                }
            }
        }
        yield return new WaitForSeconds(delay);

        for (int i = highestRowCount; i >= 0; i--)
        {
            for (int j = 0; j < MAX_COLUMNS; j++)
            {
                for (int k = boxesCache[j].Count - 1; k >= 0; k--)
                {
                    if (boxesCache[j][k].fieldPosition.y >= i)
                    {
                        if (boxesCache[j][k] != null)
                            boxesCache[j][k].DestroyBox(false, true, false, false, rowIterations * deltaDelay);
                    }
                }
            }
            rowIterations++;
        }

        boxes = new List<Box>[MAX_COLUMNS];
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            boxes[i] = new List<Box>();
        }
        columnSpawnTimers = new float[MAX_COLUMNS];
    }

    //private IEnumerator FadeIn()
    //{
    //    StopCoroutine(FadeOut());
    //    float alpha = 0f;

    // while (alpha < 1f) { alpha += Time.deltaTime; foreach (Text text in textUI) { text.color = new
    // Color(text.color.r, text.color.g, text.color.b, alpha); }

    //        foreach (Image image in imageUI)
    //        {
    //            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    //        }
    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    //private IEnumerator FadeOut()
    //{
    //    StopCoroutine(FadeIn());
    //    float alpha = 1f;

    // while (alpha > 0f) { alpha -= Time.deltaTime; foreach (Text text in textUI) { text.color = new
    // Color(text.color.r, text.color.g, text.color.b, alpha); }

    //        foreach (Image image in imageUI)
    //        {
    //            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    //        }
    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    public void BoxLanded(Box box, bool canScore)
    {
        if (gameState == GameState.Play || gameState == GameState.Boost)
        {
            if (canScore)
            {
                scoreManager.Score += 1;
                scoreManager.CreateScorePopop(box, "+1", Color.white);
            }
        }
    }

    private void BoxDeactivate(Box box, bool canScore)
    {
        if (gameState == GameState.Play)
        {
            AudioManager.Instance.PlaySoundEffect(SoundEffect.RedBoxDisable);

            if (canScore)
            {
                scoreManager.Score += 10;
                scoreManager.CreateScorePopop(box, "+10", Color.white);
            }
        }
    }

    public void BoxExploded(Box box, bool canScore)
    {
        if (gameState == GameState.Play)
        {
            if (canScore)
            {
                scoreManager.Score -= 5;
                scoreManager.CreateScorePopop(box, "-5", Color.red);
            }
        }
    }

    private bool BoxCheck(int columnIndex, int columnSize, int boxIndex)
    {
        bool hasPassed = true;

        if (boxIndex == 1 && GetDisparity() >= MAX_DISPARITY)
        {
            if (GetColumnSize(columnIndex) - GetShortestColumnIndex(true) > 3)
                hasPassed = false;
        }

        if (boxIndex == 1 && orangeBoxOverrideTimer > 0f)
            hasPassed = false;

        if (boxIndex == 1 && columnSize - GetHighestLandedColumnBoxY(GetShortestColumnIndex(true)) > 2)
        {
            hasPassed = false;
        }

        if (boxIndex == 2 && (columnIndex == 1 || columnIndex == 2 || columnIndex == 7 || columnIndex == 8))
            hasPassed = false;

        return hasPassed;
    }

    private void OffsetBoxes()
    {
        float offsetEpsilon = 0.01f;

        List<Box> boxesToDestroy = new List<Box>();
        if (isOffsetting)
        {
            CurrentOffset += ((1f - CurrentOffset) / currentOffsetDivider) * Time.deltaTime;

            if (CurrentOffset >= 1f - offsetEpsilon) //If we are finished offsetting
            {
                isOffsetting = false;
                CurrentOffset = 0f;

                for (int i = 0; i < MAX_COLUMNS; i++)
                {
                    for (int j = 0; j < boxes[i].Count; j++)
                    {
                        if (Mathf.RoundToInt(boxes[i][j].fieldPosition.y) <= 0)
                        {
                            boxesToDestroy.Add(boxes[i][j]);
                        }
                    }
                }

                for (int i = 0; i < MAX_COLUMNS; i++)
                {
                    for (int j = 0; j < boxes[i].Count; j++)
                    {
                        boxes[i][j].DownOffset();
                    }
                }
            }

            if (boxesToDestroy.Count > 0)
            {
                for (int i = boxesToDestroy.Count - 1; i >= 0; i--)
                {
                    boxesToDestroy[i].DestroyBox(false, false, false);
                }
            }
        }
    }

    private int GetPlayerColumnIndex()
    {
        if (PersistantSettings.Instance.IsMultiplayerMode()) //in multiplayer mode this returns a random players position
        {
            if (multiPlayers.Count > 0)
                return Mathf.RoundToInt(multiPlayers[Random.Range(0, multiPlayers.Count)].fieldPosition.x);
        }
        else
        {
            if (Player != null)
            {
                return Mathf.RoundToInt(Player.fieldPosition.x);
            }
        }

        return GetShortestColumnIndex(true);
    }

    private int GetRandomBoxIndex()
    {
        int totalWeight = 0;
        int boxIndex = 0;

        for (int i = 0; i < boxSpawns.Length; i++)
        {
            totalWeight += boxSpawns[i].spawnWeight;
        }

        int randomNumber = Random.Range(0, totalWeight);
        int j = 0;

        for (int i = 0; i < boxSpawns.Length; i++)
        {
            if (randomNumber >= j && randomNumber < boxSpawns[i].spawnWeight + j)
            {
                boxIndex = i;
                break;
            }
            j += boxSpawns[i].spawnWeight;
        }

        return boxIndex;
    }

    private void SpawnRandomGenBox(List<int> possibleColumnIndexes, int numberOfBoxes, int randomGenToUse) //randomGenToUse will be 1 or 2 depending on which seed to use
    {
        // int randomColumn = fileLoaderRandomBox.Next(MAX_COLUMNS - 1); List<int>
        // shortestColumnsIndexes = GetShortestColumnsIndexes();

        //int shortestColumnHeight = 999;
        //List<int> shortestColumnList = new List<int>();

        for (int i = 0; i < numberOfBoxes; i++)
        {
            //if (shortestColumnList.Count == 0)
            //{
            //    shortestColumnHeight = 999;
            //    for (int j = 0; j < possibleColumnIndexes.Count; j++)
            //    {
            //        int columnSize = GetColumnSize(possibleColumnIndexes[j]);

            //        if (columnSize < shortestColumnHeight)
            //        {
            //            shortestColumnList.Clear();
            //            shortestColumnHeight = columnSize;
            //        }
            //        if (columnSize <= shortestColumnHeight)
            //            shortestColumnList.Add(possibleColumnIndexes[j]);
            //    }
            //}

            int randomColumn = 0;

            if (randomGenToUse == 1)
                randomColumn = possibleColumnIndexes[fileLoaderRandomBox.Next(0, possibleColumnIndexes.Count - 1)];
            else if (randomGenToUse == 2)
                randomColumn = possibleColumnIndexes[fileLoaderRandomBox2.Next(0, possibleColumnIndexes.Count - 1)];
            possibleColumnIndexes.Remove(randomColumn);
            // shortestColumnList.Remove(randomColumn);

            int randomBoxType = 0;

            if (randomGenToUse == 1)
            {
                int totalBoxWeight = randomGenBoxWeight1 + randomGenBoxWeight2 + randomGenBoxWeight3 + randomGenBoxWeight4 + randomGenBoxWeight5;
                int randomWeight = fileLoaderRandomBox.Next(totalBoxWeight - 1) + 1;

                if (randomWeight > randomGenBoxWeight1)
                {
                    randomBoxType++;
                    if (randomWeight > randomGenBoxWeight1 + randomGenBoxWeight2)
                    {
                        randomBoxType++;

                        if (randomWeight > randomGenBoxWeight1 + randomGenBoxWeight2 + randomGenBoxWeight3)
                        {
                            randomBoxType++;
                            if (randomWeight > randomGenBoxWeight1 + randomGenBoxWeight2 + randomGenBoxWeight3 + randomGenBoxWeight4)
                            {
                                randomBoxType++;
                            }
                        }
                    }
                }
            }
            else if (randomGenToUse == 2)
            {
                int totalBoxWeight = randomGenBox2Weight1 + randomGenBox2Weight2 + randomGenBox2Weight3 + randomGenBox2Weight4 + randomGenBox2Weight5;
                int randomWeight = fileLoaderRandomBox2.Next(totalBoxWeight - 1) + 1;

                if (randomWeight > randomGenBox2Weight1)
                {
                    randomBoxType++;
                    if (randomWeight > randomGenBox2Weight1 + randomGenBox2Weight2)
                    {
                        randomBoxType++;

                        if (randomWeight > randomGenBox2Weight1 + randomGenBox2Weight2 + randomGenBox2Weight3)
                        {
                            randomBoxType++;
                            if (randomWeight > randomGenBox2Weight1 + randomGenBox2Weight2 + randomGenBox2Weight3 + randomGenBox2Weight4)
                            {
                                randomBoxType++;
                            }
                        }
                    }
                }
            }

            SpawnBoxNoTimer(randomColumn, randomBoxType);
        }
    }

    private void SpawnBoxNoTimer(int columnIndex, int boxIndex)
    {
        Box spawnedBox = Instantiate(boxSpawns[boxIndex].box, new Vector3(columnIndex, SPAWN_HEIGHT, 0f), Quaternion.identity) as Box;
        spawnedBox.Init(columnIndex, GetLowestFreeRowIndex(columnIndex), true);
    }

    private void SpawnBox(int columnIndex, int boxIndex)
    {
        columnSpawnTimers[columnIndex] = COLUMN_TIMER;
        if (boxIndex == 1)
            orangeBoxOverrideTimer = 1f;
        if (boxIndex == 4)
            columnSpawnTimers[columnIndex] = RED_COLUMN_TIMER;

        Box spawnedBox = Instantiate(boxSpawns[boxIndex].box, new Vector3(columnIndex, SPAWN_HEIGHT, 0f), Quaternion.identity) as Box;
        spawnedBox.Init(columnIndex, GetLowestFreeRowIndex(columnIndex), true);
    }

    private void SpawnInitBox(int columnIndex, int boxIndex)
    {
        Box initBox = Instantiate(initBoxes[boxIndex]) as Box;
        initBox.fieldPosition = new Vector2(columnIndex, SPAWN_HEIGHT);
        initBox.Init(columnIndex, 0, true);
        initBox.SetCanScore(false);
    }

    private void SpawnPlayer(float startX)
    {
        Player player = Instantiate(playerPrefab, new Vector3(startX, SPAWN_HEIGHT, 0f), Quaternion.identity) as Player;
        player.Init(startX, 4.46f);

        Player = player;
    }

    private void SpawnMultiPlayers()
    {
        int playerCount = MultiplayerInputManager.Instance.CurrentNumberOfPlayers();
        multiPlayers = new List<Player>();

        float[] spawnLocations = new float[playerCount];
        switch (playerCount)
        {
            case 2:
                spawnLocations[0] = 16f;
                spawnLocations[1] = 18f;
                break;

            case 3:
                spawnLocations[0] = 15f;
                spawnLocations[1] = 17f;
                spawnLocations[2] = 19f;
                break;

            case 4:
                spawnLocations[0] = 14f;
                spawnLocations[1] = 15f;
                spawnLocations[2] = 17f;
                spawnLocations[3] = 19f;
                break;
        }

        for (int i = 0; i < playerCount; i++)
        {
            Player player = Instantiate(playerPrefab, new Vector3(spawnLocations[i], SPAWN_HEIGHT, 0f), Quaternion.identity) as Player;
            player.Init(spawnLocations[i], 4.46f);
            player.SetMultiplayerMode(i);
            multiPlayers.Add(player);
        }
    }

    public void RespawnMultiPlayer(int playerID)
    {
        int playerX = Random.Range(2, MAX_COLUMNS - 3);
        Player player = Instantiate(playerPrefab, new Vector3(playerX, SPAWN_HEIGHT, 0f), Quaternion.identity) as Player;
        player.Init(playerX, 4.46f);
        player.SetMultiplayerMode(playerID);
        multiPlayers.Add(player);
    }

    public void CreatePlayerParticles(Vector2 centerFieldPosition, Color particleColor)
    {
        particlePoolManager.CreateParticles(centerFieldPosition, 0.5f, particleColor, true, false, 1.4f, 0.3f, 0.6f);
    }

    public void CreateBoxParticles(Vector2 centerFieldPosition, Color particleColor)
    {
        particlePoolManager.CreateParticles(centerFieldPosition, 0.5f, particleColor, false, false, 1f, 0f, 0f);
    }

    public void CreateEndingBoxParticles(Vector2 centerFieldPosition, Color particleColor)
    {
        particlePoolManager.CreateParticles(centerFieldPosition, 0.5f, particleColor, false, true, 0.6f, 0f, 0f);
    }

    public bool CheckOtherPlayersAboveCollision(int checkingPlayerID, Vector2 checkingPlayerFieldPosition, out Player collidingPlayer)
    {
        bool isTouchingAbove = false;
        Player returnPlayer = null;
        for (int i = 0; i < multiPlayers.Count; i++)
        {
            if (multiPlayers[i].GetPlayerID() != checkingPlayerID)
            {
                float yDistance = Mathf.Abs(checkingPlayerFieldPosition.y - multiPlayers[i].fieldPosition.y);
                float xDistance = Mathf.Abs(checkingPlayerFieldPosition.x - multiPlayers[i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && checkingPlayerFieldPosition.y < multiPlayers[i].fieldPosition.y)
                {
                    returnPlayer = multiPlayers[i];
                    isTouchingAbove = true;
                }
            }
        }

        collidingPlayer = returnPlayer;
        return isTouchingAbove;
    }

    public bool CheckAboveCollision(Vector2 playerFieldPosition, out Box collidingBox)
    {
        int checkColumn1 = Mathf.RoundToInt(playerFieldPosition.x - PLAYER_BOUNDS + EPSILON);
        int checkColumn2 = Mathf.RoundToInt(playerFieldPosition.x + PLAYER_BOUNDS - EPSILON);

        bool isTouchingAbove = false;
        Box returnBox = null;

        if (checkColumn1 >= 0 && checkColumn1 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn1].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn1][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn1][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 0.7f - EPSILON && playerFieldPosition.y < boxes[checkColumn1][i].fieldPosition.y + EPSILON && boxes[checkColumn1][i].IsActivated)
                {
                    // Debug.Log("CEIL " + playerPosition + " " + boxes[checkColumn1][i].transform.position);
                    returnBox = boxes[checkColumn1][i];
                    isTouchingAbove = true;
                }
            }
        }
        // else isTouchingFloor = true;

        if (checkColumn2 >= 0 && checkColumn2 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn2].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn2][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn2][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 0.7f - EPSILON && playerFieldPosition.y < boxes[checkColumn2][i].fieldPosition.y && boxes[checkColumn2][i].IsActivated)
                {
                    if (returnBox != null)
                    {
                        if (boxes[checkColumn2][i].fieldPosition.y > returnBox.fieldPosition.y)
                            returnBox = boxes[checkColumn2][i];
                    }
                    else
                        returnBox = boxes[checkColumn2][i];

                    // Debug.Log("CEIL " + playerPosition + " " + boxes[checkColumn2][i].transform.position);
                    isTouchingAbove = true;
                }
            }
        }

        collidingBox = returnBox;
        return isTouchingAbove;
    }

    public bool CheckMultiPlayerFloorCollision(int checkingPlayerID, Vector2 checkingPlayerFieldPosition, out Player collidingPlayer)
    {
        bool isTouchingFloor = false;
        Player returnPlayer = null;
        for (int i = 0; i < multiPlayers.Count; i++)
        {
            if (multiPlayers[i].GetPlayerID() != checkingPlayerID)
            {
                float yDistance = Mathf.Abs(checkingPlayerFieldPosition.y - multiPlayers[i].fieldPosition.y);
                float xDistance = Mathf.Abs(checkingPlayerFieldPosition.x - multiPlayers[i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && checkingPlayerFieldPosition.y > multiPlayers[i].fieldPosition.y)
                {
                    returnPlayer = multiPlayers[i];
                    isTouchingFloor = true;
                }
            }
        }

        //if (isTouchingFloor)
        //    AudioManager.Instance.PlaySoundEffect(SoundEffect.PlayerHitFloor);

        collidingPlayer = returnPlayer;
        return isTouchingFloor;
    }

    public bool CheckPlayerFloorCollision(Vector2 playerFieldPosition, out Box collidingBox)
    {
        int checkColumn1 = Mathf.RoundToInt(playerFieldPosition.x - PLAYER_BOUNDS + EPSILON);
        int checkColumn2 = Mathf.RoundToInt(playerFieldPosition.x + PLAYER_BOUNDS - EPSILON);
        bool isTouchingFloor = false;
        Box returnBox = null;

        if (checkColumn1 >= 0 && checkColumn1 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn1].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn1][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn1][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && playerFieldPosition.y > boxes[checkColumn1][i].fieldPosition.y && boxes[checkColumn1][i].IsActivated)
                {
                    if (boxes[checkColumn1][i] is ExplodingBox)
                    {
                        if (boxes[checkColumn1][i] != null)
                            BoxDeactivate(boxes[checkColumn1][i], true);
                        boxes[checkColumn1][i].ChangeBoxType(0);
                    }
                    returnBox = boxes[checkColumn1][i];
                    isTouchingFloor = true;
                }
            }
        }
        // else isTouchingFloor = true;

        if (checkColumn2 >= 0 && checkColumn2 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn2].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn2][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn2][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && playerFieldPosition.y > boxes[checkColumn2][i].fieldPosition.y && boxes[checkColumn2][i].IsActivated)
                {
                    if (boxes[checkColumn2][i] is ExplodingBox)
                    {
                        if (boxes[checkColumn2][i] != null)
                            BoxDeactivate(boxes[checkColumn2][i], true);
                        boxes[checkColumn2][i].ChangeBoxType(0);
                    }

                    if (returnBox != null)
                    {
                        if (boxes[checkColumn2][i].fieldPosition.y > returnBox.fieldPosition.y)
                            returnBox = boxes[checkColumn2][i];
                    }
                    else
                        returnBox = boxes[checkColumn2][i];

                    isTouchingFloor = true;
                }
            }
        }
        // else isTouchingFloor = true;

        // if (isTouchingFloor) AudioManager.Instance.PlaySoundEffect(SoundEffect.PlayerHitFloor);

        collidingBox = returnBox;
        return isTouchingFloor;
    }

    public bool CheckPlayerToPlayerSideCollision(int checkingPlayerID, Vector2 checkingPlayerFieldPosition, out bool rightSideCollision, out Player otherPlayer)
    {
        bool isTouchingWall = false;
        bool rightSide = false;
        Player otherP = null;
        for (int i = 0; i < multiPlayers.Count; i++)
        {
            if (multiPlayers[i].GetPlayerID() != checkingPlayerID)
            {
                float yDistance = Mathf.Abs(checkingPlayerFieldPosition.y - multiPlayers[i].fieldPosition.y);
                float xDistance = Mathf.Abs(checkingPlayerFieldPosition.x - multiPlayers[i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON)
                {
                    isTouchingWall = true;
                    rightSide = multiPlayers[i].fieldPosition.x > checkingPlayerFieldPosition.x;
                    otherP = multiPlayers[i];
                }
            }
        }
        otherPlayer = otherP;
        rightSideCollision = rightSide;
        return isTouchingWall;
    }

    //isRight = are we checking to the right of the player?
    public bool CheckPlayerWallCollision(Vector2 playerFieldPosition, out Box collidingBoxLeft, out Box collidingBoxRight)
    {
        int checkColumn1 = Mathf.RoundToInt(playerFieldPosition.x - 0.5f + EPSILON);
        int checkColumn2 = Mathf.RoundToInt(playerFieldPosition.x + 0.5f - EPSILON);
        bool isTouchingWall = false;
        Box returnBoxLeft = null;
        Box returnBoxRight = null;

        if (checkColumn1 >= 0 && checkColumn1 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn1].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn1][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn1][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && boxes[checkColumn1][i].IsActivated)
                {
                    isTouchingWall = true;
                    returnBoxLeft = boxes[checkColumn1][i];

                    isTouchingWall = true;
                    returnBoxLeft = boxes[checkColumn1][i];
                    returnBoxLeft = boxes[checkColumn1][i];
                    // Debug.Log("WALL " + isRight + " " + playerPosition.y + " " + boxes[checkColumn][i].transform.position.y);
                }
            }
        }
        else
            isTouchingWall = true;

        if (checkColumn2 >= 0 && checkColumn2 < MAX_COLUMNS)
        {
            for (int i = 0; i < boxes[checkColumn2].Count; i++)
            {
                float yDistance = Mathf.Abs(playerFieldPosition.y - boxes[checkColumn2][i].fieldPosition.y);
                float xDistance = Mathf.Abs(playerFieldPosition.x - boxes[checkColumn2][i].fieldPosition.x);
                if (yDistance < 1f - EPSILON && xDistance < 1f - EPSILON && boxes[checkColumn2][i].IsActivated)
                {
                    isTouchingWall = true;
                    returnBoxRight = boxes[checkColumn2][i];

                    isTouchingWall = true;
                    returnBoxRight = boxes[checkColumn2][i];
                    returnBoxRight = boxes[checkColumn2][i];
                    // Debug.Log("WALL " + isRight + " " + playerPosition.y + " " + boxes[checkColumn][i].transform.position.y);
                }
            }
        }
        else
            isTouchingWall = true;

        collidingBoxLeft = returnBoxLeft;
        collidingBoxRight = returnBoxRight;
        return isTouchingWall;
    }

    public void Explode(int columnIndexCenter, int rowIndexCenter)
    {
        List<Box> destroyList = new List<Box>();

        if (gameState != GameState.MenuWithFallingBlocks)
            AudioManager.Instance.PlaySoundEffect(SoundEffect.RedBoxExplosion);

        for (int i = Mathf.Clamp(columnIndexCenter - 1, 0, MAX_COLUMNS - 1); i <= Mathf.Clamp(columnIndexCenter + 1, 0, MAX_COLUMNS - 1); i++)
        {
            for (int j = 0; j < boxes[i].Count; j++)
            {
                for (int k = Mathf.Clamp(rowIndexCenter - 1, 0, MAX_ROWS - 1); k <= Mathf.Clamp(rowIndexCenter + 1, 0, MAX_ROWS - 1); k++)
                {
                    if (GetClosestBoxRow(boxes[i][j].fieldPosition) == k && boxes[i][j].IsActivated && boxes[i][j].DestinationRowIndex > 0 + (isOffsetting ? 1 : 0))
                    {
                        destroyList.Add(boxes[i][j]);
                        // boxes[i][j].DestroyBox(true);
                    }
                }
            }
        }
        foreach (Box box in destroyList)
        {
            if (box is ExplodingBox)
            {
                ((ExplodingBox)box).Explode();
            }
            box.DestroyBox(true, true, false);
        }

        for (int i = Mathf.Clamp(columnIndexCenter - 1, 0, MAX_COLUMNS - 1); i <= Mathf.Clamp(columnIndexCenter + 1, 0, MAX_COLUMNS - 1); i++)
        {
            UpdateColumn(i, Mathf.Clamp(rowIndexCenter - 1, 0, MAX_ROWS - 1));
            UpdateBeams(i);
        }
    }

    private int GetTallestColumnIndex(bool fromRight)
    {
        int tallestColumnIndex = 0;
        int tallestColumnHeight = 0;

        if (!fromRight)
        {
            for (int i = 0; i < MAX_COLUMNS; i++)
            {
                int columnSize = GetColumnSize(i);
                if (columnSize >= tallestColumnHeight)
                {
                    tallestColumnHeight = columnSize;
                    tallestColumnIndex = i;
                }
            }
        }
        else
        {
            for (int i = MAX_COLUMNS - 1; i >= 0; i--)
            {
                int columnSize = GetColumnSize(i);
                if (columnSize >= tallestColumnHeight)
                {
                    tallestColumnHeight = columnSize;
                    tallestColumnIndex = i;
                }
            }
        }

        return tallestColumnIndex;
    }

    private int GetShortestColumnIndex(bool fromRight)
    {
        int shortestColumnIndex = 0;
        int shortestColumnHeight = MAX_ROWS;

        if (!fromRight)
        {
            for (int i = 0; i < MAX_COLUMNS; i++)
            {
                int columnSize = GetColumnSize(i);
                if (columnSize <= shortestColumnHeight)
                {
                    shortestColumnHeight = columnSize;
                    shortestColumnIndex = i;
                }
            }
        }
        else
        {
            for (int i = MAX_COLUMNS - 1; i >= 0; i--)
            {
                int columnSize = GetColumnSize(i);
                if (columnSize <= shortestColumnHeight)
                {
                    shortestColumnHeight = columnSize;
                    shortestColumnIndex = i;
                }
            }
        }

        return shortestColumnIndex;
    }

    private List<int> GetShortestColumnsIndexes()
    {
        List<int> shortestColumnsIndexes = new List<int>();
        int shortestColumnHeight = MAX_ROWS;

        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            int columnSize = GetColumnSize(i);
            if (columnSize < shortestColumnHeight)
            {
                shortestColumnHeight = columnSize;
            }
        }

        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            int columnSize = GetColumnSize(i);
            if (columnSize == shortestColumnHeight)
            {
                shortestColumnsIndexes.Add(i);
            }
        }

        return shortestColumnsIndexes;
    }

    public void UpdateColumn(int columnIndex, int minRowIndex)
    {
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            if (GetClosestBoxRow(boxes[columnIndex][i].fieldPosition) >= minRowIndex)
            {
                boxes[columnIndex][i].DestinationRowIndex = 0;
                boxes[columnIndex][i].Fall();
            }
        }
    }

    public int GetClosestBoxRow(Vector2 boxFieldPosition)
    {
        return Mathf.Clamp(Mathf.RoundToInt(boxFieldPosition.y), 0, MAX_ROWS - 1);
    }

    private Box GetTopBox(int columnIndex)
    {
        Box topBox = boxes[columnIndex][0];

        foreach (Box box in boxes[columnIndex])
        {
            if (box.fieldPosition.y > topBox.fieldPosition.y)
                topBox = box;
        }

        return topBox;
    }

    //Lowest row index that is not taken in the indicated column index
    public int GetLowestFreeRowIndex(int columnIndex)
    {
        int lowestFreeRow = 0;
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            if (Mathf.RoundToInt(boxes[columnIndex][i].fieldPosition.y) >= lowestFreeRow && i != MAX_ROWS - 1 && !boxes[columnIndex][i].InAir)
            {
                lowestFreeRow = boxes[columnIndex][i].DestinationRowIndex + 1;
            }
        }
        return lowestFreeRow;
    }

    public bool CheckLandedOnBox(int columnIndex, Box checkingBox, Vector2 checkingBoxTPosition, out Box otherbox)
    {
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            if (boxes[columnIndex][i] != null)
            {
                float distance = Mathf.Abs(checkingBoxTPosition.y - boxes[columnIndex][i].transform.position.y);
                if (distance < 1f && boxes[columnIndex][i] != checkingBox && checkingBoxTPosition.y > boxes[columnIndex][i].transform.position.y)
                {
                    // Debug.Log(distance + " " + checkingBoxTPosition.y + " " + boxes[columnIndex][i].transform.position.y);
                    otherbox = boxes[columnIndex][i];
                    return true;
                }
            }
        }
        otherbox = null;
        return false;
    }

    public int GetColumnSize(int columnIndex)
    {
        return boxes[columnIndex].Count;
    }

    ////Return the column size of boxes that have landed and are activated
    //public int GetActiveLandedColumnSize(int columnIndex)
    //{
    //    int columnSize = 0;

    //    for (int i = 0; i < boxes[columnIndex].Count; i++)
    //    {
    //        columnSize += !boxes[columnIndex][i].InAir && boxes[columnIndex][i].IsActivated ? 1 : 0;
    //    }
    //    return columnSize;
    //}

    public int GetHighestLandedColumnBoxY(int columnIndex)
    {
        int highestY = 0;

        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            int y = Mathf.RoundToInt(boxes[columnIndex][i].fieldPosition.y);
            if (y > highestY && !boxes[columnIndex][i].InAir && boxes[columnIndex][i].IsActivated)
                highestY = y;
        }
        return highestY;
    }

    private int GetRowSize(int rowIndex)
    {
        int rowSize = 0;

        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            for (int j = 0; j < boxes[i].Count; j++)
            {
                if (!boxes[i][j].InAir && Mathf.RoundToInt(boxes[i][j].fieldPosition.y) == rowIndex && !(boxes[i][j] is ExplodingBox)
                    )
                {
                    rowSize++;
                }
            }
        }
        return rowSize;
    }

    //Give beams correct offset
    public void UpdateBeams(int columnIndex)
    {
        for (int i = 0; i < boxes[columnIndex].Count; i++)
        {
            boxes[columnIndex][i].UpdateBeams();
        }
    }

    public void ButtonClick()
    {
        AudioManager.Instance.PlayClick();
    }
}

public enum GameMode
{
    Endless,
    LevelFromFile,
    MusicLevelFromFile
}

public class LevelRowData
{
    public int rowTimer;
    public string[] boxNames; //x for random, o for orange, etc.
    public MusicNoteData note;
    public int[] boxWeights;
    public int[] boxWeights2;
    public int numberOfRandomBoxesToSpawn;
    public int numberOfRandomBoxes2ToSpawn;
    public bool weights1Changed;
    public bool weights2Changed;
}

public class MusicNoteData
{
    public string noteName = "";
    public float fadeOutTime;
}