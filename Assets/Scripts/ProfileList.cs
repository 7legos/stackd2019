﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProfileList : MonoBehaviour {
    [SerializeField]
    private Color myColor, evenColor, oddColor;
    [SerializeField]
    private Text place;
    [SerializeField]
    private Image profilePic;
    [SerializeField]
    private Text username;
    [SerializeField]
    private Text score;
    [SerializeField]
    private Image crown;
    [SerializeField]
    private Color firstPlace, secondPlace, thirdPlace;

    [HideInInspector]
    public string profileID;

    private Image background;

    void Awake()
    {
        background = GetComponent<Image>();
    }

    public void SetInfo(int place, string name, long score, string profileID)
    {
        this.place.text = place.ToString();
        username.text = name;
        this.score.text = score.ToString();
        this.profileID = profileID;

        if (place % 2 == 0)
            background.color = evenColor;
        else background.color = oddColor;

        SetCrownColor();
    }

    private void SetCrownColor()
    {
        if (int.Parse(place.text) == 1)
            crown.color = firstPlace;
        else
        if (int.Parse(place.text) == 2)
            crown.color = secondPlace;
        else
        if (int.Parse(place.text) == 3)
            crown.color = thirdPlace;
        else
            crown.color = new Color(0f, 0f, 0f, 0f);
    }

    public void SetPicture(Texture2D image)
    {
        profilePic.sprite = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.zero);
    }

    //Indicate this is *our* score
    public void SetMyProfile()
    {
        background.color = myColor;
        username.text = "You";
        score.text = PlayerPrefs.GetInt("highScore",0).ToString();
    }
}
