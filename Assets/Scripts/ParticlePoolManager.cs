﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct ParticleSettings
{
    public int particleCount;
    public float minScale;
    public float maxScale;
    public float maxRotationAngle;
    public float minDistance;
    public float maxDistance;
}

public class ParticlePoolManager : MonoBehaviour
{
    public static ParticlePoolManager Instance;

    [SerializeField]
    private int maxParticleCount;

    [SerializeField]
    private Particle particlePrefab;

    [SerializeField]
    private ParticleSettings boxSettings;

    [SerializeField]
    private ParticleSettings playerSettings;

    [SerializeField]
    private ParticleSettings endingBoxSettings;

    private List<Particle> inactiveParticleList;

    // Use this for initialization
    private void Start()
    {
        DontDestroyOnLoad(this);
        Instance = this;
        inactiveParticleList = new List<Particle>();

        GenerateParticles();
    }

    private void Update()
    {
        if (inactiveParticleList.Count < maxParticleCount)
            GenerateParticles();
    }

    private void GenerateParticles(int count = 100)
    {
        for (int i = 0; i < count; i++)
        {
            Particle particle = Instantiate(particlePrefab, Vector3.left * 99999f, Quaternion.identity) as Particle;
            particle.Init(this);
            inactiveParticleList.Add(particle);
            particle.gameObject.SetActive(false);
        }
    }

    private Particle Pop()
    {
        Particle poppedParticle;
        if (inactiveParticleList.Count > 0)
        {
            poppedParticle = inactiveParticleList[0];
            inactiveParticleList.Remove(poppedParticle);
        }
        else
        {
            poppedParticle = Instantiate(particlePrefab, Vector3.left * 99999f, Quaternion.identity) as Particle;
            poppedParticle.Init(this);
            poppedParticle.gameObject.SetActive(false);
        }
        return poppedParticle;
    }

    public void Push(Particle particle)
    {
        inactiveParticleList.Add(particle);
        particle.transform.position = Vector3.left * 99999f;
        particle.gameObject.SetActive(false);
    }

    public void CreateParticles(Vector3 centerPosition, float radius, Color color, bool isPlayer, bool isEnding, float speed, float dragDelta, float initDrag)
    {
        if (!isEnding)
        {
            if (isPlayer)
            {
                for (int i = 0; i < playerSettings.particleCount; i++)
                {
                    Particle particle = Pop();
                    if (particle != null)
                    {
                        Vector3 particlePosition = new Vector3(centerPosition.x + Random.Range(-radius, radius), centerPosition.y + Random.Range(-radius, radius), centerPosition.z);
                        // Vector3 particleDestination = particlePosition +
                        // (Vector3)Random.insideUnitSphere *
                        // Random.Range(playerSettings.minDistance, playerSettings.maxDistance);
                        Vector3 particleDestination = particlePosition + (particlePosition - centerPosition).normalized * Random.Range(playerSettings.minDistance, playerSettings.maxDistance);
                        float particleRotation = Random.Range(-playerSettings.maxRotationAngle, playerSettings.maxRotationAngle);
                        particleDestination.z = 0f;
                        particle.gameObject.SetActive(true);
                        particle.Activate(particlePosition, particleDestination, Random.Range(playerSettings.minScale, playerSettings.maxScale), particleRotation, color, speed, dragDelta, initDrag);
                    }
                }
            }
            else
            {
                for (int i = 0; i < boxSettings.particleCount; i++)
                {
                    Particle particle = Pop();
                    if (particle != null)
                    {
                        Vector3 particlePosition = new Vector3(centerPosition.x + Random.Range(-radius, radius), centerPosition.y + Random.Range(-radius, radius), centerPosition.z);
                        // Vector3 particleDestination = particlePosition +
                        // (Vector3)Random.onUnitSphere * Random.Range(boxSettings.minDistance, boxSettings.maxDistance);
                        Vector3 particleDestination = particlePosition + (particlePosition - centerPosition).normalized * Random.Range(boxSettings.minDistance, boxSettings.maxDistance);
                        float particleRotation = Random.Range(-boxSettings.maxRotationAngle, boxSettings.maxRotationAngle);
                        particleDestination.z = 0f;
                        particle.gameObject.SetActive(true);
                        particle.Activate(particlePosition, particleDestination, Random.Range(boxSettings.minScale, boxSettings.maxScale), particleRotation, color, speed, dragDelta, initDrag);
                    }
                }
            }
        }
        else //isEnding
        {
            for (int i = 0; i < endingBoxSettings.particleCount; i++)
            {
                Particle particle = Pop();
                if (particle != null)
                {
                    Vector3 particlePosition = new Vector3(centerPosition.x + Random.Range(-radius, radius), centerPosition.y + Random.Range(-radius, radius), centerPosition.z);
                    Vector3 particleDestination = particlePosition + (particlePosition - centerPosition).normalized * Random.Range(endingBoxSettings.minDistance, endingBoxSettings.maxDistance);
                    float particleRotation = Random.Range(-endingBoxSettings.maxRotationAngle, endingBoxSettings.maxRotationAngle);
                    particleDestination.z = 0f;
                    particle.gameObject.SetActive(true);
                    particle.Activate(particlePosition, particleDestination, Random.Range(endingBoxSettings.minScale, endingBoxSettings.maxScale), particleRotation, color, speed, dragDelta, initDrag);
                }
            }
        }
    }
}