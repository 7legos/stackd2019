﻿using UnityEngine;
using System.Collections;

public class Player : FieldObject
{
    [SerializeField]
    private Beam beamPrefab;

    [SerializeField]
    private Sprite multiplayerBoxSprite;

    [SerializeField]
    private PlayerReviver playerReviverPrefab;

    [SerializeField]
    private float currentVelocity = 0f;

    private bool rightPressed = false;
    private bool leftPressed = false;
    private bool jumpPressed = false;
    private bool autoJump = false;
    private bool canControl = false;
    private bool isDead = false;
    private Beam myBeam;
    private bool multiplayerMode = false;

    private int playerID = 0; //for multiplayer only

    private Vector2 lastFieldPosition;
    private bool hasLandedBefore;

    public void Init(float startX, float destinationY)
    {
        lastFieldPosition = fieldPosition;
        myBeam = Instantiate(beamPrefab, new Vector3(startX, GameplayManager.SPAWN_HEIGHT), Quaternion.identity) as Beam;
        myBeam.InitPlayer(startX, destinationY);
    }

    public void SetMultiplayerMode(int playerID)
    {
        this.playerID = playerID;
        multiplayerMode = true;
        GetComponent<SpriteRenderer>().sprite = multiplayerBoxSprite;
        GetComponent<SpriteRenderer>().color = MultiplayerInputManager.Instance.GetPlayerColor(playerID);
        myBeam.SetColor(MultiplayerInputManager.Instance.GetPlayerColor(playerID));
    }

    public void Step(float scale)
    {
        rightPressed = false;
        leftPressed = false;
        jumpPressed = false;
        if (!isDead)
            currentVelocity -= GameplayManager.PLAYER_VELOCITY_DELTA / 3000f * scale;

        Vector2 touchPosition = Vector2.zero;

        if (canControl && !GameplayManager.Instance.IsUISelected())
        {
            if (multiplayerMode)
            {
                if (MultiplayerInputManager.Instance.CheckInput(playerID, InputAction.Left)) leftPressed = true;
                if (MultiplayerInputManager.Instance.CheckInput(playerID, InputAction.Right)) rightPressed = true;
                if (MultiplayerInputManager.Instance.CheckInput(playerID, InputAction.Jump)) jumpPressed = true;
                if (MultiplayerInputManager.Instance.CheckInput(playerID, InputAction.ToggleJumpKey)) autoJump = !autoJump;

                if (autoJump)
                    jumpPressed = true;
            }
            else
            { //single player controls
                if (Input.GetKey("a")) leftPressed = true;
                if (Input.GetKey("d")) rightPressed = true;
                if (Input.GetKey("w")) jumpPressed = true;
                if (Input.GetKey("s")) autoJump = true;
            }

            foreach (Touch touch in Input.touches)
            {
                float distance = Mathf.Abs(touch.position.x / Screen.width - Camera.main.WorldToScreenPoint(transform.position).x / Screen.width);

                if (touch.position.x / Screen.width > 0.5f) rightPressed = true;
                if (touch.position.x / Screen.width < 0.5f) leftPressed = true;

                touchPosition = touch.position;
            }

            if ((leftPressed || rightPressed) && GameplayManager.Instance.GetGameState() != GameState.GameOver && hasLandedBefore)
                GameplayManager.Instance.StartGame();
        }

        // float distanceToFinger = Mathf.Abs(Camera.main.ScreenToWorldPoint(touchPosition).x -
        // transform.position.x); //In world coords float horizontalDelta =
        // Mathf.Min(GameplayManager.PLAYER_HORIZONTAL_SPEED, distanceToFinger);

        Vector2 movementVector = new Vector2((leftPressed ? -GameplayManager.PLAYER_HORIZONTAL_SPEED : 0) + (rightPressed ? GameplayManager.PLAYER_HORIZONTAL_SPEED : 0), currentVelocity);
        Vector2 toMovePosition = fieldPosition + movementVector;

        Box collidingBox = null;
        Player collidingPlayer = null;
        bool isAbove = false;
        bool isGrounded = false;

        if (GameplayManager.Instance.CheckPlayerFloorCollision(toMovePosition - (Vector2.right * movementVector.x), out collidingBox))
        {
            canControl = true;
            isGrounded = true;
            toMovePosition = new Vector2(toMovePosition.x, collidingBox.fieldPosition.y + 1f);
            fieldPosition = new Vector2(fieldPosition.x, collidingBox.fieldPosition.y + 1f);
            if (jumpPressed)
                currentVelocity = .21f;
            else
                currentVelocity = 0f;

            if (isAbove)
            {
                Die(lastFieldPosition, true, false);
            }
        }

        if (multiplayerMode)
        {
            if (GameplayManager.Instance.CheckMultiPlayerFloorCollision(playerID, toMovePosition - (Vector2.right * movementVector.x), out collidingPlayer))
            {
                canControl = true;
                isGrounded = true;
                toMovePosition = new Vector2(toMovePosition.x, collidingPlayer.fieldPosition.y + 1f + GameplayManager.EPSILON);
                fieldPosition = new Vector2(fieldPosition.x, collidingPlayer.fieldPosition.y + 1f + GameplayManager.EPSILON);
                if (jumpPressed)
                    currentVelocity = .21f;
                else
                    currentVelocity = 0f;

                if (isAbove)
                {
                    Die(lastFieldPosition, true, false);
                }
            }
        }

        if (GameplayManager.Instance.CheckAboveCollision(toMovePosition - (Vector2.right * movementVector.x), out collidingBox))
        {
            isAbove = true;
            currentVelocity = -collidingBox.CurrentVelocity;
            toMovePosition = new Vector2(toMovePosition.x, collidingBox.fieldPosition.y - 1f - GameplayManager.EPSILON);
            fieldPosition = new Vector2(fieldPosition.x, collidingBox.fieldPosition.y - 1f - GameplayManager.EPSILON);
            if (isGrounded || GameplayManager.Instance.CheckPlayerFloorCollision(toMovePosition - (Vector2.right * movementVector.x), out collidingBox))
            {
                Die(lastFieldPosition, true, false);
            }
        }

        if (multiplayerMode)
        {
            collidingPlayer = null;
            if (GameplayManager.Instance.CheckOtherPlayersAboveCollision(playerID, toMovePosition - (Vector2.right * movementVector.x), out collidingPlayer))
            {
                isAbove = true;
                // currentVelocity = -collidingBox.CurrentVelocity;
                collidingPlayer.fieldPosition = new Vector2(collidingPlayer.fieldPosition.x, fieldPosition.y + 1f + GameplayManager.EPSILON);
                // toMovePosition = new Vector2(toMovePosition.x, collidingBox.fieldPosition.y - 1f -
                // GameplayManager.EPSILON); fieldPosition = new Vector2(fieldPosition.x,
                // collidingBox.fieldPosition.y - 1f - GameplayManager.EPSILON); if (isGrounded ||
                // GameplayManager.Instance.CheckPlayerFloorCollision(toMovePosition - (Vector2.right
                // * movementVector.x), out collidingBox)) { Die(lastFieldPosition); }
            }
        }

        Box collidingBoxLeft = null;
        Box collidingBoxRight = null;
        Vector3 pass = toMovePosition;
        // bool checkRight = movementVector.x > 0f + GameplayManager.EPSILON;
        if (GameplayManager.Instance.CheckPlayerWallCollision(pass, out collidingBoxLeft, out collidingBoxRight))
        {
            if (collidingBoxLeft != null)
            {
                fieldPosition = new Vector2(collidingBoxLeft.fieldPosition.x + 1f + GameplayManager.EPSILON, fieldPosition.y);
                toMovePosition = new Vector2(collidingBoxLeft.fieldPosition.x + 1f + GameplayManager.EPSILON, toMovePosition.y);
            }
            else if (collidingBoxRight != null)
            {
                fieldPosition = new Vector2(collidingBoxRight.fieldPosition.x - 1f + GameplayManager.EPSILON, fieldPosition.y);
                toMovePosition = new Vector2(collidingBoxRight.fieldPosition.x - 1f + GameplayManager.EPSILON, toMovePosition.y);
            }
            else //We hit a boundry
            {
                fieldPosition = new Vector2((fieldPosition.x > 0f ? 29f - GameplayManager.EPSILON : -5f + GameplayManager.EPSILON), fieldPosition.y);
                toMovePosition = new Vector2((fieldPosition.x > 0f ? 29f - GameplayManager.EPSILON : -5f + GameplayManager.EPSILON), toMovePosition.y);
            }
        }

        if (multiplayerMode)
        {
            collidingPlayer = null;
            bool rightSideCollision; //if false, was left side
            if (GameplayManager.Instance.CheckPlayerToPlayerSideCollision(playerID, toMovePosition, out rightSideCollision, out collidingPlayer))
            {
                if (collidingPlayer.fieldPosition.y - fieldPosition.y > 0.5f) //force other players above us
                {
                    collidingPlayer.fieldPosition = new Vector2(collidingPlayer.fieldPosition.x, fieldPosition.y + 1f + GameplayManager.EPSILON);
                }
                else
                {
                    if (rightSideCollision) //box is to our right
                    {
                        fieldPosition = new Vector2(collidingPlayer.fieldPosition.x - 1f + GameplayManager.EPSILON, fieldPosition.y);
                        toMovePosition = new Vector2(collidingPlayer.fieldPosition.x - 1f + GameplayManager.EPSILON, toMovePosition.y);
                    }
                    else
                    {
                        fieldPosition = new Vector2(collidingPlayer.fieldPosition.x + 1f + GameplayManager.EPSILON, fieldPosition.y);
                        toMovePosition = new Vector2(collidingPlayer.fieldPosition.x + 1f + GameplayManager.EPSILON, toMovePosition.y);
                    }
                }
            }
        }

        //Fix sporadic movements
        if (Vector2.Distance(toMovePosition, lastFieldPosition) > 1.45f)
        {
            fieldPosition = lastFieldPosition;
        }
        else
        {
            fieldPosition = toMovePosition;
        }

        if (isGrounded)
            hasLandedBefore = true;

        lastFieldPosition = fieldPosition;

        if (GameplayManager.Instance.GetGameState() == GameState.Boost)
            fieldPosition = new Vector2(-0.5f, fieldPosition.y);
    }

    public int GetPlayerID()
    {
        return playerID;
    }

    public bool IsDead()
    {
        return isDead;
    }

    public void Die(Vector2 particlesFieldPosition, bool openMenu, bool overrideInvincible)
    {
        if (GameplayManager.Instance.GetInvincibleMode() && !overrideInvincible)
        {
            fieldPosition = new Vector2(fieldPosition.x, fieldPosition.y + 1.3f);
        }
        else
        {
            if (!isDead)
            {
                isDead = true;
                Color playerColor = multiplayerMode ? MultiplayerInputManager.Instance.GetPlayerColor(playerID) : Color.white;
                GameplayManager.Instance.CreatePlayerParticles(particlesFieldPosition, playerColor);
                GameplayManager.Instance.OnDeath(this, openMenu);

                if (PersistantSettings.Instance.IsMultiplayerMode() && GameplayManager.Instance.NumberOfPlayersAlive() > 0)
                {
                    DownButNotOut();
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }

    //you need to be revived by a teammate quickly before you die
    private void DownButNotOut()
    {
        PlayerReviver playerReviver = Instantiate(playerReviverPrefab, transform.position, Quaternion.identity);
        playerReviver.Initialize(playerID, 7.5f, GetComponent<SpriteRenderer>().color);
        Destroy(this.gameObject);
    }
}