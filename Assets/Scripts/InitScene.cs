﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InitScene : MonoBehaviour
{
    void Start()
    {
        PlayerPrefs.SetInt("currentScore", 0);
        PlayerPrefs.SetInt("init", 1);
        SceneManager.LoadScene("Transition");
    }
}