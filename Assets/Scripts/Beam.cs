﻿using UnityEngine;
using System.Collections;
using System;

public class Beam : FieldObject
{
    public int ColumnIndex;
    public int DestinationRowIndex;

    private SpriteRenderer mySpriteRenderer;
    private bool hasLanded = false;

    //private float currentVelocity;
    private float alpha = 0f;

    private float fadeDelay = 0.4f;
    private bool isPlayer = false;
    private float startX = 0f;
    private float destinationY = 0f;
    private float fadeOutSpeed = 1.6f;
    private float fadeInSpeed = 3.4f;
    private bool quickFadeMode;
    private bool fadingIn = true;

    protected override void Awake()
    {
        base.Awake();
        mySpriteRenderer = GetComponent<SpriteRenderer>();

        alpha = mySpriteRenderer.color.a;
    }

    public void Init(int newColumnIndex, int newDestinationRowIndex)
    {
        ColumnIndex = newColumnIndex;
        DestinationRowIndex = newDestinationRowIndex;
        // currentVelocity = GameplayManager.BEAM_START_VELOCITY;
    }

    public void InitPlayer(float startX, float destinationY)
    {
        this.destinationY = destinationY;
        this.startX = startX;
        isPlayer = true;
        // currentVelocity = GameplayManager.BEAM_START_VELOCITY;
    }

    public void SetColor(Color color)
    {
        mySpriteRenderer.color = color;
    }

    protected override void Update()
    {
        base.Update();
        Move();
        Fade();
    }

    private void Move()
    {
        fieldPosition = new Vector2(ColumnIndex, -1f);
        if (isPlayer)
            fieldPosition = new Vector2(startX, -1f);

        //DestinationRowIndex = 0;
        //if (hasLanded)
        //{
        //    currentVelocity = 0f;
        //    fieldPosition = new Vector2(ColumnIndex, DestinationRowIndex - 1f);

        //    if (isPlayer)
        //        fieldPosition = new Vector2(startX, 3.46f);
        //}

        //if (!hasLanded) //If we are still falling (havent reached destination row yet)
        //{
        //    currentVelocity += GameplayManager.BEAM_VELOCITY_DELTA * Time.deltaTime;
        //    fieldPosition = new Vector2(ColumnIndex, fieldPosition.y - (currentVelocity * Time.deltaTime));

        //    if (isPlayer)
        //        fieldPosition = new Vector2(startX, fieldPosition.y);
        //}

        //if (!isPlayer)
        //{
        //    if (fieldPosition.y <= DestinationRowIndex - 1f + GameplayManager.EPSILON && !hasLanded) //Landed (called once)
        //    {
        //        currentVelocity = 0f;
        //        hasLanded = true;
        //        fieldPosition = new Vector3(ColumnIndex, DestinationRowIndex - 1f);
        //    }
        //}
        //else
        //{
        //    if (fieldPosition.y <= destinationY && !hasLanded) //Landed (called once)
        //    {
        //        currentVelocity = 0f;
        //        hasLanded = true;
        //        fieldPosition = new Vector3(startX, destinationY);
        //    }
        //}
    }

    private void Fade()
    {
        //if (!hasLanded && !quickFadeMode)
        //    return;

        //fadeDelay -= Time.deltaTime;

        //if (fadeDelay > 0f)
        //    return;

        //alpha -= fadeSpeed * Time.deltaTime;
        //mySpriteRenderer.color = new Color(mySpriteRenderer.color.r, mySpriteRenderer.color.g, mySpriteRenderer.color.b, alpha);

        //if (alpha <= 0f)
        //{
        //    Destroy(this.gameObject);
        //}

        if (fadingIn)
        {
            alpha += fadeInSpeed * Time.deltaTime;

            if (alpha > 1f)
            {
                alpha = 1f;
                fadingIn = false;
            }
        }
        else
        {
            fadeDelay -= Time.deltaTime;
            if (fadeDelay < 0f)
                alpha -= fadeOutSpeed * Time.deltaTime;
        }

        mySpriteRenderer.color = new Color(mySpriteRenderer.color.r, mySpriteRenderer.color.g, mySpriteRenderer.color.b, alpha);
        if (alpha < 0f)
        {
            Destroy(this.gameObject);
        }
    }

    public void QuickFadeOut()
    {
        quickFadeMode = true;
        fadeDelay = -1f;
        fadeOutSpeed = 3.5f;
    }
}