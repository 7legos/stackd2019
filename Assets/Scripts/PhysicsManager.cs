﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhysicsManager : MonoBehaviour
{
    public static PhysicsManager Instance;
    public const float TIME_STEP = 1f / 60f;

    private float lastTime;
    private List<Box>[] boxesCache;

    private void Awake()
    {
        lastTime = Time.time;
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;

        if (Instance == null)
            Instance = this;
        else
            Debug.LogError("More than 1 PhysicsManager detected");
    }

    private void Update()
    {
        float workTime = Time.time - lastTime;

        if (!GameplayManager.Instance.IsPaused)
        {
            while (workTime >= TIME_STEP)
            {
                SimulateStep(1f);
                workTime -= TIME_STEP;
            }

            SimulateStep(workTime / TIME_STEP);
        }

        lastTime = Time.time;
    }

    private void SimulateStep(float scale)
    {
        if (PersistantSettings.Instance.IsMultiplayerMode())
        {
            if (scale > 0.9f)
                GameplayManager.Instance.MultiPlayerStep(scale);
        }
        else
        {
            if (GameplayManager.Instance.Player != null)
            {
                if (scale > 0.9f)
                    GameplayManager.Instance.Player.Step(scale);
            }
        }

        boxesCache = GameplayManager.Instance.Boxes;

        for (int i = GameplayManager.MAX_COLUMNS - 1; i >= 0; i--)
        {
            for (int j = boxesCache[i].Count - 1; j >= 0; j--)
            {
                if (boxesCache[i][j] != null)
                {
                    boxesCache[i][j].MoveStep(scale);
                    boxesCache[i][j].UpdateStep(scale);
                }
            }
        }
    }
}