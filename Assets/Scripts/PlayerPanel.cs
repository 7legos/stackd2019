﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
    [SerializeField]
    private Text playerTitle, waitingText, readyText;
    [SerializeField]
    private Image playerPreviewImage;
    [SerializeField]
    private GameObject waitingForInputPanel, inputControlledPanel;

    private bool waitingForPlayer = true;
    private bool isReady = false;
    private int playerIndex;

    void Awake()
    {
        playerTitle.text = "Waiting...";
        waitingText.text = "Press\n" + MultiplayerInputManager.Instance.GetNextAvailableJoinButton() + "\nTo Join";
    }

    public void AttachPlayer(int playerIndex)
    {
        if (waitingForPlayer)
        {
            this.playerIndex = playerIndex;
            playerTitle.text = "Player " + (playerIndex + 1);
            waitingForPlayer = false;
            waitingForInputPanel.SetActive(false);
            inputControlledPanel.SetActive(true);
            playerPreviewImage.color = MultiplayerInputManager.Instance.GetPlayerColor(playerIndex);
            SetReadyCheck(true);
        }
        else
        {
            Debug.LogError("PlayerPanel already has a player attached!");
        }
    }

    void Update()
    {
        CheckForReadyToggle();
    }

    private void CheckForReadyToggle()
    {
        if (!waitingForPlayer)
        {
            if (MultiplayerInputManager.Instance.CheckInput(playerIndex, InputAction.JumpDownPressOnly))
            {
                ToggleReadyCheck();
            }
        }
    }

    private void ToggleReadyCheck()
    {
        SetReadyCheck(!isReady);
    }

    private void SetReadyCheck(bool ready)
    {
        isReady = ready;
        if (isReady)
        {
            readyText.text = "Ready";
            readyText.color = Color.green;
        }
        else
        {
            readyText.text = "Not Ready";
            readyText.color = Color.red;
        }
    }

    public bool IsWaitingForPlayer()
    {
        return waitingForPlayer;
    }

    public bool IsReady()
    {
        return isReady;
    }
}
