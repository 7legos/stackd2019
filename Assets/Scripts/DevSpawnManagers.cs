﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevSpawnManagers : MonoBehaviour
{
    [SerializeField]
    private PersistantSettings persistantSettingsPrefab;

    [SerializeField]
    private AudioManager audioManagerPrefab;

    [SerializeField]
    private MultiplayerInputManager multiplayerInputManagerPrefab;

    [SerializeField]
    private ParticlePoolManager particlePoolManagerPrefab;

    [SerializeField]
    private ResourceManager resourceManagerPrefab;

    //make sure our managers are initialized when running directly from Unity editor
    private void Awake()
    {
        if (PersistantSettings.Instance == null)
            Instantiate(persistantSettingsPrefab);
        if (AudioManager.Instance == null)
            Instantiate(audioManagerPrefab);
        if (MultiplayerInputManager.Instance == null)
            Instantiate(multiplayerInputManagerPrefab);
        if (ParticlePoolManager.Instance == null)
            Instantiate(particlePoolManagerPrefab);
        if (ResourceManager.Instance == null)
            Instantiate(resourceManagerPrefab);
    }
}