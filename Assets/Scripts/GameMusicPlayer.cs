﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMusicPlayer : MonoBehaviour
{
    [SerializeField]
    private AudioSource myAudioSource;

    public void StartSong()
    {
        myAudioSource.Play();
    }
}